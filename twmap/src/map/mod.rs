use crate::datafile;

use bitflags::bitflags;
use fixed::types::{I17F15, I22F10, I27F5};
use image::RgbaImage;
use ndarray::Array2;
use serde::{Deserialize, Serialize};
use structview::View;
use thiserror::Error;

use std::io;

pub mod checks;
/// Complex methods for map structs
pub mod edit;
/// Simple impl blocks for map structs and traits
mod impls;
mod load;
/// MapDir format implementation
pub mod map_dir;
pub mod parse;
mod save;

pub use load::{Load, LoadMultiple};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
#[serde(rename_all = "lowercase", tag = "type")]
pub enum Version {
    DDNet06,
    Teeworlds07,
}

/// The TwMap struct represents a Teeworlds 0.7 map or a DDNet 0.6 map.
/// Which one of those it is will always be determined during the parsing process and is stored in the `version` field.
///
/// The library cares a lot about the integrity of the struct.
/// The [`check`](struct.TwMap.html#method.check) method verifies that all limitations are met.
///
/// # Parsing
///
/// TwMap has several different parsing methods: [`parse`](struct.TwMap.html#method.parse), [`parse_path`](struct.TwMap.html#method.parse_path), [`parse_file`](struct.TwMap.html#method.parse_file), [`parse_dir`](struct.TwMap.html#method.parse_dir), [`parse_datafile`](struct.TwMap.html#method.parse_datafile)
///
/// Each of them execute the [`check`](struct.TwMap.html#method.check) method to finalize the process.
///
/// If you want to leave out the checks, you can use the `_unchecked` variation of that parsing method if it is provided.
/// Note that the `_unchecked` variation might also exclude some common fixes.
///
/// # Saving
///
/// TwMap can save maps in the binary format ([`save`](struct.TwMap.html#method.save), [`save_file`](struct.TwMap.html#method.save_file)) and in the MapDir format ([`save_dir`](struct.TwMap.html#method.save_dir)).
///
/// Each saving method will first execute [`check`](struct.TwMap.html#method.check), if the map fails the check, it will not be saved.
///
/// # Loading
///
/// When loading a map from the binary format, a lot of data will be decompressed in the process.
/// Since this is the main slowdown factor, some larger data chunks will be left compressed.
/// The compressed parts are the `data` field in [`Image`](struct.Image.html)s and [`Sound`](struct.Sound.html)s and the `tiles` field in tilemap layers.
/// If you want to save the map at the end anyways, then you can simply use the [`load`](struct.TwMap.html#method.load) method on the entire map.
/// If not, use the `load` method only on the images, sounds and tilemap layers that you want to use.
///
/// **Note:**
/// - you can also use `load` on slices and vectors of Images, Sounds, Layers, Groups with layers,
/// - some methods rely on having parts of the map loaded, especially more abstract methods like [`mirror`](struct.TwMap.html#method.mirror) and [`rotate_right`](struct.TwMap.html#method.rotate_right)
/// - if you want to leave out the checks on the decompressed data, you can use the `load_unchecked` methods
///
/// # Fixed Point Integers
///
/// In many parts of the struct fixed point integers are used.
/// They are represented using the crate `fixed`.
/// Position and size fixed point integers are always chosen so that 1 always translates to the width of 1 tile.
/// The other usages are usually chosen so that 0 = 0%, 1 = 100%.

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TwMap {
    pub version: Version,
    pub info: Info,
    pub images: Vec<Image>,
    pub envelopes: Vec<Envelope>,
    pub groups: Vec<Group>,
    pub sounds: Vec<Sound>,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Map - {0}")]
    Map(#[from] checks::MapError),
    #[error("Map from Datafile - {0}")]
    MapFromDatafile(#[from] parse::MapFromDatafileError),
    #[error("Datafile saving - {0}")]
    DatafileSaving(#[from] datafile::SizeError),
    #[error("Datafile parsing - {0}")]
    DatafileParse(#[from] datafile::DatafileParseError),
    #[error("IO - {0}")]
    Io(#[from] io::Error),
    #[error("MapDir - {0}")]
    Dir(#[from] map_dir::MapDirError),
}

const MAPRES_06_ENV: &str = "TWMAP_MAPRES_06";
const MAPRES_07_ENV: &str = "TWMAP_MAPRES_07";

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
// identifier for all relevant types of layers
pub enum LayerKind {
    Game,
    Tiles,
    Quads,
    Front,
    Tele,
    Speedup,
    Switch,
    Tune,
    Sounds,
    Invalid(InvalidLayerKind),
}

#[derive(Debug, Eq, PartialOrd, PartialEq, Copy, Clone, Hash)]
pub enum InvalidLayerKind {
    Unknown(i32),        // unknown value of 'LAYERTYPE' identifier
    UnknownTilemap(i32), // 'LAYERTYPE' identified a tile layer, unknown value of 'TILESLAYERFLAG' identifier
    NoType,              // layer item too short to get 'LAYERTYPE' identifier
    NoTypeTilemap, // 'LAYERTYPE' identified a tile layer, layer item too short to get 'TILESLAYERFLAG' identifier
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
// identifies type of a map item by type_id
pub enum ItemType {
    Version,
    Info,
    Image,
    Envelope,
    Group,
    Layer,
    EnvPoints,
    Sound,
    AutoMapper,
    Unknown(u16),
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
// for holding compressed data, since decompression is expensive
pub enum CompressedData<T, U> {
    Compressed(Vec<u8>, usize, U),
    Loaded(T),
}

#[derive(Debug, Default, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Info {
    pub author: String,
    pub version: String,
    pub credits: String,
    pub license: String,
    pub settings: Vec<String>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Image {
    External(ExternalImage),
    Embedded(EmbeddedImage),
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct ExternalImage {
    #[serde(skip)]
    pub name: String, // file name sanitized

    pub size: Point<u32>,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct ImageLoadInfo {
    pub size: Point<u32>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct EmbeddedImage {
    /// Translates directly to a filename ({name}.png).
    /// The file name must be sanitized.
    pub name: String,

    pub image: CompressedData<RgbaImage, ImageLoadInfo>, // None if the image is external (not embedded)
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Group {
    pub name: String,
    // offsets the layer from the upper left corner of the map (in tiles)
    pub offset: Point<I27F5>,
    // parallax of the group
    pub parallax: Point<i32>,
    // which layers belong to the group
    #[serde(skip)]
    pub layers: Vec<Layer>,
    // lets the group's layers disappear at some distance
    pub clipping: bool,
    pub clip: Point<I27F5>,
    pub clip_size: Point<I27F5>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Sound {
    /// Translates directly to a filename ({name}.opus).
    /// The file name must be sanitized.
    pub name: String,
    pub data: CompressedData<Vec<u8>, ()>,
}

#[derive(Default, Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct AutomapperConfig {
    pub config: Option<u16>,
    pub seed: u32,
    pub automatic: bool,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Default, Serialize, Deserialize)]
pub struct BezierCurve<T> {
    pub in_tangent_dx: T,
    pub in_tangent_dy: T,
    pub out_tangent_dx: T,
    pub out_tangent_dy: T,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type")]
/// Belongs to a envelope point.
/// Describes the change in value from this point to the next.
///
/// For math: `frac` is where the interpolate between the two points (0 is at first point, 1 at second, 0.5 halfway),
/// The resulting value then mixes the two values: `left + (right - left) * result`.
pub enum CurveKind<T> {
    /// Value of first point until second point, abrupt change there.
    Step,
    /// Linear interpolation between the two points.
    /// Math: `frac`
    Linear,
    /// First slow, later much faster value change.
    /// Math: `frac^3`
    Slow,
    /// First fast, later much slower value change.
    /// Math: `1 - (1 - frac)^3`
    Fast,
    /// Slow, faster then once more slow value change.
    /// Math: `3 * frac^2 - 2 * frac^3`
    Smooth,
    /// Very flexible curve, each channel individually.
    /// Uses bezier curves for interpolation.
    /// Only kind of curve where every channel gets their own interpolation values.
    Bezier(BezierCurve<T>),
    /// For compatibility, will error on check.
    Unknown(i32),
}

// TODO for 0.9.0: Add BezierDefault trait requirement for T
#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct EnvPoint<T> {
    /// Time stamp of this point in milliseconds, must not be negative
    pub time: i32,
    pub content: T,
    #[serde(flatten)]
    pub curve: CurveKind<T>,
}

/// Default bezier tangent values are all zeroes.
/// This trait implements the constructors for those values.
/// only I32Color needs to implement this manually, since volume (i32) and Position are already all zeroes in their default values.
pub trait BezierDefault: Default {
    fn bezier_default() -> Self {
        Default::default()
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize, Default)]
pub struct Position {
    #[serde(flatten)]
    pub offset: Point<I17F15>,
    pub rotation: I22F10, // any value, sets rotation in degrees (positive -> clockwise)
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
// each r g b a value is any i32, but will be internally divided by 1024
pub struct I32Color {
    pub r: I22F10,
    pub g: I22F10,
    pub b: I22F10,
    pub a: I22F10,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct Volume(pub I22F10);

#[derive(Default, Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Env<T: Copy> {
    pub name: String,
    /// Whether the envelope should be synchronized across clients by using server time.
    /// Will use client time otherwise
    pub synchronized: bool,
    /// Must be in chronological order based on their time
    pub points: Vec<EnvPoint<T>>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type")]
// to allow all envelope types to coexist in a vector
pub enum Envelope {
    Position(Env<Position>),
    Color(Env<I32Color>),
    Sound(Env<Volume>),
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum EnvelopeKind {
    Position,
    Color,
    Sound,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TilesLoadInfo {
    pub size: Point<u32>,
    pub compression: bool,
}

pub struct OutdatedTileVersion {
    pub bytes_per_tile: usize,
    pub convert_fnc: fn(&[u8]) -> Vec<u8>,
}

unsafe impl View for TileFlags {} // deriving directly didn't work
bitflags! {
    #[repr(C)]
    #[derive(Default, Serialize, Deserialize)]
    #[serde(into = "map_dir::DirTileFlags", try_from = "map_dir::DirTileFlags")]
    pub struct TileFlags: u8 {
        const FLIP_V = 0b0001;
        const FLIP_H = 0b0010;
        const OPAQUE = 0b0100;
        const ROTATE = 0b1000;
    }
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct Tile {
    // for TilesLayer
    pub id: u8,
    #[serde(flatten)]
    pub flags: TileFlags,
    #[serde(skip)]
    pub(crate) skip: u8, // used for 0.7 tile compression
    #[serde(skip)]
    pub(crate) unused: u8,
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct GameTile {
    // for GameLayer and FrontLayer
    pub id: u8,
    #[serde(flatten)]
    pub flags: TileFlags,
    #[serde(skip)]
    pub(crate) skip: u8, // used for 0.7 tile compression
    #[serde(skip)]
    pub(crate) unused: u8,
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct Tele {
    pub number: u8,
    pub id: u8,
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
#[serde(into = "i16", from = "i16")]
/// Required to make the Speedup tile struct 1-byte-aligned
pub struct I16 {
    pub(crate) bytes: [u8; 2],
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct Speedup {
    pub force: u8,
    pub max_speed: u8,
    pub id: u8,
    #[serde(skip)]
    pub(crate) unused_padding: u8, // padding, unused
    /// Angle, value between 0 and (exclusive) 360
    /// 0 points right, angle increases clock-wise
    pub angle: I16,
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct Switch {
    pub number: u8,
    pub id: u8,
    #[serde(flatten)]
    pub flags: TileFlags,
    pub delay: u8,
}

#[derive(Debug, Copy, Clone, View, Eq, PartialEq, Default, Serialize, Deserialize)]
#[repr(C)]
pub struct Tune {
    pub number: u8,
    pub id: u8,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct GameLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<GameTile>, TilesLoadInfo>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct FrontLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<GameTile>, TilesLoadInfo>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct TeleLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<Tele>, TilesLoadInfo>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct SpeedupLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<Speedup>, TilesLoadInfo>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct SwitchLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<Switch>, TilesLoadInfo>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct TuneLayer {
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<Tune>, TilesLoadInfo>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct TilesLayer {
    pub name: String,
    pub detail: bool,
    pub color: Color,
    #[serde(with = "map_dir::envelope_index_serialization")]
    pub color_env: Option<u16>,
    pub color_env_offset: i32,
    #[serde(with = "map_dir::image_index_serialization")]
    pub image: Option<u16>,
    #[serde(flatten, with = "map_dir::tiles_serialization")]
    pub tiles: CompressedData<Array2<Tile>, TilesLoadInfo>,
    pub automapper_config: AutomapperConfig,
}

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[repr(C)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for Point<T> {}
unsafe impl<T: bytemuck::Pod> bytemuck::Pod for Point<T> {}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct Quad {
    pub corners: [Point<I17F15>; 4], // top-left, top-right, bottom-left, bottom-right
    pub position: Point<I17F15>,
    pub colors: [Color; 4],
    pub texture_coords: [Point<I22F10>; 4], // represents the stretching done by shift+dragging of corners in the editor
    #[serde(with = "map_dir::envelope_index_serialization")]
    pub position_env: Option<u16>,
    pub position_env_offset: i32,
    #[serde(with = "map_dir::envelope_index_serialization")]
    pub color_env: Option<u16>,
    pub color_env_offset: i32,
}

#[derive(Default, Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct QuadsLayer {
    pub name: String,
    pub detail: bool,
    pub quads: Vec<Quad>,
    #[serde(with = "map_dir::image_index_serialization")]
    pub image: Option<u16>, // index to its image
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum SoundShape {
    Rectangle { size: Point<I17F15> },
    Circle { radius: I27F5 },
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct SoundSource {
    pub position: Point<I17F15>,
    pub looping: bool,
    pub panning: bool,
    pub delay: i32,
    pub falloff: u8,
    #[serde(with = "map_dir::envelope_index_serialization")]
    pub position_env: Option<u16>,
    pub position_env_offset: i32,
    #[serde(with = "map_dir::envelope_index_serialization")]
    pub sound_env: Option<u16>,
    pub sound_env_offset: i32,
    pub shape: SoundShape,
}

#[derive(Default, Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct SoundsLayer {
    pub name: String,
    pub detail: bool,
    pub sources: Vec<SoundSource>,
    #[serde(with = "map_dir::sound_index_serialization")]
    pub sound: Option<u16>,
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum Layer {
    Game(GameLayer),
    Tiles(TilesLayer),
    Quads(QuadsLayer),
    Front(FrontLayer),
    Tele(TeleLayer),
    Speedup(SpeedupLayer),
    Switch(SwitchLayer),
    Tune(TuneLayer),
    Sounds(SoundsLayer),
    #[serde(skip)]
    Invalid(InvalidLayerKind),
}

pub trait TilemapLayer: AnyLayer {
    type TileType: AnyTile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo>;

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo>;
}

pub trait AnyTile: Default + PartialEq + Copy + Clone + checks::TileChecking + View {
    fn id(&self) -> u8;

    fn flags(&self) -> Option<TileFlags>;

    /// Only true for `Tile` and `GameTile`
    fn supports_vanilla_compression() -> bool {
        false
    }
}

pub trait AnyLayer: Sized {
    fn kind() -> LayerKind;

    fn get(layer: &Layer) -> Option<&Self>;

    fn get_mut(layer: &mut Layer) -> Option<&mut Self>;
}

/// Marker trait, implemented for all physics layers
pub trait PhysicsLayer: TilemapLayer {}
