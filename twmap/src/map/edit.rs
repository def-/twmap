use crate::constants;
use crate::convert::{To, TryTo};
use crate::map::*;

use fixed::traits::LosslessTryFrom;
use fixed::types::{I17F15, I27F5};
use image::{io::Reader as ImageReader, ImageError, ImageFormat, Pixel, RgbaImage};
use ndarray::{s, Array2};

use std::cmp::min;
use std::env;
use std::fs;
use std::io;
use std::path::Path;

impl TwMap {
    /// Returns a reference to the specified physics layer, if the map contains it.
    /// Note that every map must have a Game layer to pass the checks.
    pub fn find_physics_layer<T: PhysicsLayer>(&self) -> Option<&T> {
        match self
            .physics_group()
            .layers
            .iter()
            .rev()
            .find(|l| l.kind() == T::kind())
        {
            None => None,
            Some(l) => T::get(l),
        }
    }

    /// Returns a mutable reference to the specified physics layer, if the map contains it.
    /// Note that every map must have a Game layer to pass the checks.
    pub fn find_physics_layer_mut<T: PhysicsLayer>(&mut self) -> Option<&mut T> {
        match self
            .physics_group_mut()
            .layers
            .iter_mut()
            .rev()
            .find(|l| l.kind() == T::kind())
        {
            None => None,
            Some(l) => T::get_mut(l),
        }
    }
}

impl LayerKind {
    // returns index suitable for checking for duplicates
    const fn duplicate_index(&self) -> usize {
        match self {
            LayerKind::Game => 0,
            LayerKind::Front => 1,
            LayerKind::Switch => 2,
            LayerKind::Tele => 3,
            LayerKind::Speedup => 4,
            LayerKind::Tune => 5,
            _ => panic!(), // may exist multiple times
        }
    }
}

impl TwMap {
    pub fn parse_path_unchecked<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let path = path.as_ref();

        let metadata = fs::metadata(path)?;
        if metadata.is_file() {
            TwMap::parse_file_unchecked(path)
        } else if metadata.is_dir() {
            TwMap::parse_dir_unchecked(path)
        } else {
            Err(io::Error::new(io::ErrorKind::InvalidData, "Neither a file nor directory").into())
        }
    }

    /// Parses binary as well as MapDir maps.
    pub fn parse_path<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let map = TwMap::parse_path_unchecked(&path)?;
        map.check()?;
        Ok(map)
    }
}

impl Sound {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Sound, opus_headers::ParseError> {
        let path = path.as_ref();
        let name = path
            .file_stem()
            .unwrap()
            .to_str()
            .ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "The file name includes invalid utf-8",
                )
            })?
            .to_owned();
        let buf = std::fs::read(path)?;
        opus_headers::parse_from_read(&buf[..])?;
        Ok(Sound {
            name,
            data: buf.into(),
        })
    }
}

impl EmbeddedImage {
    /// Creates a embedded image using a provided image file.
    /// This function supports png and any other file formats you activate via features of the crate `image`.
    /// In your `Cargo.toml` you should turn off the default features of `image` by setting `default-features = false`.
    /// Then you can activate different file formats by activating specific features.
    /// Errors with an io::ErrorKind::InvalidInput if the filename includes invalid utf-8
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<EmbeddedImage, ImageError> {
        let path = path.as_ref();
        let mut reader = ImageReader::open(path)?;
        reader.set_format(ImageFormat::Png);
        let image = reader.decode()?.into_rgba8().into();
        let name = match path.file_stem().unwrap().to_str() {
            Some(str) => str.to_owned(),
            None => {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "The file name includes invalid utf-8",
                )
                .into())
            }
        };
        Ok(EmbeddedImage { name, image })
    }
}

impl Version {
    fn mapres_paths(&self) -> &'static [&'static str] {
        match self {
            Version::DDNet06 => &[
                "/usr/share/ddnet/data/mapres",
                "/usr/share/games/ddnet/data/mapres",
                "/usr/local/share/ddnet/data/mapres",
                "/usr/local/share/games/ddnet/data/mapres",
                "/usr/pkg/share/ddnet/data/mapres",
                "/usr/pkg/share/games/ddnet/data/mapres",
                "/opt/ddnet/data/mapres",
            ],
            Version::Teeworlds07 => &[
                "/usr/share/teeworlds/data/mapres",
                "/usr/share/games/teeworlds/data/mapres",
                "/usr/local/share/teeworlds/data/mapres",
                "/usr/local/share/games/teeworlds/data/mapres",
                "/usr/pkg/share/teeworlds/data/mapres",
                "/usr/pkg/share/games/teeworlds/data/mapres",
                "/opt/teeworlds/data/mapres",
            ],
        }
    }
}

impl ExternalImage {
    /// Tries to embed the external images by loading them from the file `<mapres_directory>/<image_name>`
    pub fn embed<P: AsRef<Path>>(&self, mapres_directory: P) -> Result<EmbeddedImage, ImageError> {
        let mut file_name = self.name.clone();
        file_name.push_str(".png");

        let path = mapres_directory.as_ref().join(&file_name);

        EmbeddedImage::from_file(path)
    }
}

impl TwMap {
    /// Tries to embed the external images by loading them from the file `<mapres_directory>/<image_name>`
    pub fn embed_images<P: AsRef<Path>>(&mut self, mapres_directory: P) -> Result<(), ImageError> {
        let mut result = Ok(());
        for image in &mut self.images {
            if let Image::External(ex) = image {
                match ex.embed(mapres_directory.as_ref()) {
                    Ok(emb) => *image = Image::Embedded(emb),
                    Err(err) => {
                        if result.is_ok() {
                            result = Err(err);
                        }
                    }
                }
            }
        }
        result
    }

    /// Tries to figure out the path from where to embed the external images automatically.
    /// Will first look into the environment variables Version::mapres_env.
    /// If that isn't set, it will look for a Teeworlds/DDNet installation (only works on linux).
    pub fn embed_images_auto(&mut self) -> Result<(), ImageError> {
        match env::var_os(self.version.mapres_env()) {
            Some(path) => self.embed_images(&path),
            None => {
                for path in self.version.mapres_paths() {
                    if let Ok(metadata) = fs::metadata(path) {
                        if metadata.is_dir() {
                            return self.embed_images(path);
                        }
                    }
                }
                Err(ImageError::IoError(io::Error::new(
                    io::ErrorKind::Other,
                    format!("Error finding mapres directory for version {:?}: The environment variable {} isn't set and neither can a client installation be found (only possible on linux)", self.version, self.version.mapres_env())
                )))
            }
        }
    }
}

impl TwMap {
    /// Removes duplicate physics layers of each type.
    /// Note that every map must have at most 1 physics layer of each type to pass the checks.
    /// The removal process prioritizes removing the layers in front.
    pub fn remove_duplicate_physics_layers(&mut self) -> u16 {
        let mut does_exist = [false; 7];
        let mut removed = 0;

        for group in self.groups.iter_mut().rev() {
            for i in (0..group.layers.len()).rev() {
                let variant = group.layers[i].kind();
                if variant.is_physics_layer() {
                    if does_exist[variant.duplicate_index()] {
                        group.layers.remove(i);
                        removed += 1;
                    }
                    does_exist[variant.duplicate_index()] = true;
                }
            }
        }

        removed
    }
}

fn all_tiles_default<T: AnyTile>(tiles: &Array2<T>) -> bool {
    tiles.iter().all(|t| *t == T::default())
}

impl Layer {
    /// Returns `true` if the layer has no content.
    /// - Tile map layers -> all tiles zeroed
    /// - Quads layers -> no Quads
    /// - Sounds layers -> no SoundSources
    pub fn is_empty(&self) -> bool {
        use Layer::*;
        match self {
            Game(_) => false, // Game layer must not be removed
            Tiles(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Quads(l) => l.quads.is_empty(),
            Front(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Tele(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Speedup(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Switch(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Tune(l) => all_tiles_default(l.tiles.unwrap_ref()),
            Sounds(l) => l.sources.is_empty(),
            Invalid(_) => false,
        }
    }
}

impl TwMap {
    /// Combination of every other `remove_unused` method.
    pub fn remove_everything_unused(&mut self) -> u32 {
        let mut removed = 0;
        removed += self.remove_unused_layers().to::<u32>();
        removed += self.remove_unused_groups().to::<u32>();
        let (removed_external, removed_embedded) = self.remove_unused_images();
        removed += removed_external.to::<u32>();
        removed += removed_embedded.to::<u32>();
        removed += self.remove_unused_envelopes().to::<u32>();
        removed += self.remove_unused_sounds().to::<u32>();
        removed
    }

    /// Removes all layers for which [`is_empty`](enum.Layer.html#method.is_empty) returns `true`.
    pub fn remove_unused_layers(&mut self) -> u16 {
        let mut removed = 0;
        for group in &mut self.groups {
            let mut i = 0;
            while i < group.layers.len() {
                if group.layers[i].is_empty() && group.layers[i].kind() != LayerKind::Game {
                    removed += 1;
                    group.layers.remove(i);
                } else {
                    i += 1;
                }
            }
        }
        removed
    }

    /// Removes all groups that contain zero layers.
    pub fn remove_unused_groups(&mut self) -> u16 {
        let mut removed = 0;
        let mut i = 0;
        while i < self.groups.len() {
            if self.groups[i].layers.is_empty() {
                removed += 1;
                self.groups.remove(i);
            } else {
                i += 1;
            }
        }
        removed
    }

    /// Returns `true` if the image index is set for a tiles layer or a quad.
    pub fn is_image_in_use(&self, index: u16) -> bool {
        for group in &self.groups {
            for layer in &group.layers {
                match layer {
                    Layer::Tiles(l) => {
                        if l.image == Some(index) {
                            return true;
                        }
                    }
                    Layer::Quads(l) => {
                        if l.image == Some(index) {
                            return true;
                        }
                    }
                    _ => {}
                }
            }
        }
        false
    }

    /// For easy remapping of all image indices in tiles layers and quads.
    pub fn edit_image_indices(&mut self, edit_fn: impl Fn(Option<u16>) -> Option<u16>) {
        for group in &mut self.groups {
            for layer in &mut group.layers {
                match layer {
                    Layer::Tiles(l) => l.image = edit_fn(l.image),
                    Layer::Quads(l) => l.image = edit_fn(l.image),
                    _ => {}
                }
            }
        }
    }

    /// Removes all images for which [`is_image_in_use`](struct.TwMap.html#method.is_image_in_use) returns `true`.
    /// Return value: (count of removed external images, count of removed embedded images)
    pub fn remove_unused_images(&mut self) -> (u16, u16) {
        let mut removed_external = 0;
        let mut removed_embedded = 0;
        let mut i = 0;
        while i < self.images.len() {
            if self.is_image_in_use(i.try_to()) {
                i += 1;
            } else {
                let removed_image = self.images.remove(i);
                match removed_image {
                    Image::External(_) => removed_external += 1,
                    Image::Embedded(_) => removed_embedded += 1,
                }
                self.edit_image_indices(|index| {
                    index.map(|index| {
                        if index.to::<usize>() > i {
                            index - 1
                        } else {
                            index
                        }
                    })
                })
            }
        }
        (removed_external, removed_embedded)
    }

    /// Returns `true` if the sound index is set for a sounds layer.
    pub fn is_sound_in_use(&self, index: u16) -> bool {
        for group in &self.groups {
            for layer in &group.layers {
                if let Layer::Sounds(l) = layer {
                    if l.sound == Some(index) {
                        return true;
                    }
                }
            }
        }
        false
    }

    /// For easy remapping of all sound indices in sounds layers.
    pub fn edit_sound_indices(&mut self, edit_fn: impl Fn(Option<u16>) -> Option<u16>) {
        for group in &mut self.groups {
            for layer in &mut group.layers {
                if let Layer::Sounds(l) = layer {
                    l.sound = edit_fn(l.sound)
                }
            }
        }
    }

    /// Removes all sounds for which [`is_sound_in_use`](struct.TwMap.html#method.is_sound_in_use) returns `true`.
    pub fn remove_unused_sounds(&mut self) -> u16 {
        let mut removed = 0;
        let mut i = 0;
        while i < self.sounds.len() {
            if self.is_sound_in_use(i.try_to()) {
                i += 1;
            } else {
                removed += 1;
                self.sounds.remove(i);
                self.edit_sound_indices(|index| {
                    index.map(|index| {
                        if index.to::<usize>() > i {
                            index - 1
                        } else {
                            index
                        }
                    })
                })
            }
        }
        removed
    }

    /// Returns `true` if the envelope index is in use.
    pub fn is_env_in_use(&self, index: u16) -> bool {
        for group in &self.groups {
            for layer in &group.layers {
                use Layer::*;
                match layer {
                    Tiles(l) => {
                        if l.color_env == Some(index) {
                            return true;
                        }
                    }
                    Quads(l) => {
                        for quad in &l.quads {
                            if quad.color_env == Some(index) {
                                return true;
                            }
                            if quad.position_env == Some(index) {
                                return true;
                            }
                        }
                    }
                    Sounds(l) => {
                        for source in &l.sources {
                            if source.position_env == Some(index) {
                                return true;
                            }
                            if source.sound_env == Some(index) {
                                return true;
                            }
                        }
                    }
                    _ => {}
                }
            }
        }
        false
    }

    /// For easy remapping of all envelope indices in tiles, quads and sounds layers.
    pub fn edit_env_indices(&mut self, edit_fn: impl Fn(Option<u16>) -> Option<u16>) {
        for group in &mut self.groups {
            for layer in &mut group.layers {
                use Layer::*;
                match layer {
                    Tiles(l) => l.color_env = edit_fn(l.color_env),
                    Quads(l) => {
                        for quad in &mut l.quads {
                            quad.color_env = edit_fn(quad.color_env);
                            quad.position_env = edit_fn(quad.position_env);
                        }
                    }
                    Sounds(l) => {
                        for source in &mut l.sources {
                            source.position_env = edit_fn(source.position_env);
                            source.sound_env = edit_fn(source.sound_env);
                        }
                    }
                    _ => {}
                }
            }
        }
    }

    /// Removes all sounds for which [`is_env_in_use`](struct.TwMap.html#method.is_env_in_use) returns `true`.
    pub fn remove_unused_envelopes(&mut self) -> u16 {
        let mut removed = 0;
        let mut i = 0;
        while i < self.envelopes.len() {
            if self.is_env_in_use(i.try_to()) {
                i += 1;
            } else {
                removed += 1;
                self.envelopes.remove(i);
                self.edit_env_indices(|index| {
                    index.map(|index| {
                        if index.to::<usize>() > i {
                            index - 1
                        } else {
                            index
                        }
                    })
                })
            }
        }
        removed
    }
}

/// Returns the table for the OPAQUE tile flag, derived from the image data
pub fn calc_opaque_table(image: &RgbaImage) -> [[bool; 16]; 16] {
    let tile_width = image.width() / 16;
    let tile_height = image.height() / 16;
    let mut table = [[false; 16]; 16];
    if tile_width != tile_height {
        return table;
    }

    for tile_y in 0..16 {
        let tile_y_pos = tile_y * tile_width;
        for tile_x in 0..16 {
            let tile_x_pos = tile_x * tile_width;
            let mut opaque = true;

            'outer: for pixel_y in 0..tile_width {
                for pixel_x in 0..tile_width {
                    let y = tile_y_pos + pixel_y;
                    let x = tile_x_pos + pixel_x;
                    if image.get_pixel(x, y).channels()[3] < 250 {
                        opaque = false;
                        break 'outer;
                    }
                }
            }

            table[tile_y.try_to::<usize>()][tile_x.try_to::<usize>()] = opaque;
        }
    }
    table[0][0] = false; // top left tile is manually emptied
    table
}

impl Image {
    /// Returns the table for the OPAQUE tile flag of the image.
    pub fn calc_opaque_table(&self, version: Version) -> [[bool; 16]; 16] {
        match self {
            Image::External(image) => {
                constants::external_opaque_table(&image.name, version).unwrap_or([[false; 16]; 16])
            }
            Image::Embedded(image) => calc_opaque_table(image.image.unwrap_ref()),
        }
    }
}

impl TwMap {
    /// Fill in all OPAQUE tile flags.
    pub fn process_tile_flag_opaque(&mut self) {
        let tables: Vec<[[bool; 16]; 16]> = self
            .images
            .iter()
            .map(|image| image.calc_opaque_table(self.version))
            .collect();
        for group in &mut self.groups {
            for layer in &mut group.layers {
                if let Layer::Tiles(layer) = layer {
                    let opaque_table = {
                        if layer.color.a != 255 {
                            &[[false; 16]; 16]
                        } else {
                            match layer.image {
                                None => &[[false; 16]; 16],
                                Some(index) => &tables[index.to::<usize>()],
                            }
                        }
                    };
                    let tiles = layer.tiles.unwrap_mut();
                    for tile in tiles {
                        let opaque_value =
                            opaque_table[tile.id.to::<usize>() / 16][tile.id.to::<usize>() % 16];
                        tile.flags.set(TileFlags::OPAQUE, opaque_value);
                    }
                }
            }
        }
    }

    /// Set the width and height of external images to their default values.
    pub fn set_external_image_dimensions(&mut self) {
        for image in &mut self.images {
            if let Image::External(ex) = image {
                if let Some(size) = constants::external_dimensions(&ex.name, self.version) {
                    ex.size = size;
                }
            }
        }
    }
}

pub trait EditTile {
    fn tile(_tile: &mut Tile) {}
    fn game_tile(_tile: &mut GameTile) {}
    fn tele(_tele: &mut Tele) {}
    fn speedup(_speedup: &mut Speedup) {}
    fn switch(_switch: &mut Switch) {}
    fn tune(_tune: &mut Tune) {}
}

fn edit_tilemap<T: TilemapLayer, F: Fn(&mut T::TileType)>(layer: &mut T, f: F) {
    layer.tiles_mut().unwrap_mut().iter_mut().for_each(f)
}

impl TwMap {
    /// Requires the tiles to be loaded
    pub fn edit_tiles<T: EditTile>(&mut self) {
        for group in &mut self.groups {
            for layer in &mut group.layers {
                match layer {
                    Layer::Game(l) => edit_tilemap(l, T::game_tile),
                    Layer::Tiles(l) => edit_tilemap(l, T::tile),
                    Layer::Front(l) => edit_tilemap(l, T::game_tile),
                    Layer::Tele(l) => edit_tilemap(l, T::tele),
                    Layer::Speedup(l) => edit_tilemap(l, T::speedup),
                    Layer::Switch(l) => edit_tilemap(l, T::switch),
                    Layer::Tune(l) => edit_tilemap(l, T::tune),
                    Layer::Quads(_) | Layer::Sounds(_) | Layer::Invalid(_) => {}
                }
            }
        }
    }
}

pub struct ZeroUnusedParts;

impl TileFlags {
    fn clear_unused(&mut self) -> bool {
        let cleared = *self & TileFlags::all();
        let changed = cleared != *self;
        *self = cleared;
        changed
    }

    fn clear_unused_and_opaque(&mut self) -> bool {
        let cleared = *self & (TileFlags::all() - TileFlags::OPAQUE);
        let changed = cleared != *self;
        *self = cleared;
        changed
    }
}

impl EditTile for ZeroUnusedParts {
    fn tile(tile: &mut Tile) {
        tile.flags.clear_unused();
        tile.unused = 0;
        tile.skip = 0;
    }
    fn game_tile(tile: &mut GameTile) {
        tile.flags.clear_unused_and_opaque();
        tile.unused = 0;
        tile.skip = 0;
    }
    fn speedup(speedup: &mut Speedup) {
        speedup.unused_padding = 0;
    }
    fn switch(switch: &mut Switch) {
        switch.flags.clear_unused_and_opaque();
    }
}

fn zero_air_tile<T: AnyTile>(tile: &mut T) {
    if tile.id() == 0 {
        *tile = T::default()
    }
}

/// To zero out tiles with id 0 completely
pub struct ZeroAir;

impl EditTile for ZeroAir {
    fn tile(tile: &mut Tile) {
        zero_air_tile(tile)
    }
    fn game_tile(tile: &mut GameTile) {
        zero_air_tile(tile)
    }
    fn tele(tele: &mut Tele) {
        zero_air_tile(tele)
    }
    fn speedup(speedup: &mut Speedup) {
        zero_air_tile(speedup)
    }
    fn switch(switch: &mut Switch) {
        zero_air_tile(switch)
    }
    fn tune(tune: &mut Tune) {
        zero_air_tile(tune)
    }
}

/// Extend 2D ndarray in each direction by the specified amount.
/// Copies the outer edges into the new region.
/// This simulates the behavior of the end of tilemaps in gameplay, since the outer edges will continue to infinity.
///
/// Returns None if the specified extend amounts cause a integer overflow
pub fn edge_extend_ndarray<T: Default + Copy>(
    ndarray: &Array2<T>,
    up_left: Point<usize>,
    down_right: Point<usize>,
) -> Option<Array2<T>> {
    let up = up_left.y;
    let down = down_right.y;
    let left = up_left.x;
    let right = down_right.x;
    let old_height = ndarray.nrows();
    let new_height = old_height.checked_add(up)?.checked_add(down)?;
    let old_width = ndarray.ncols();
    let new_width = old_width.checked_add(left)?.checked_add(right)?;
    let mut new_ndarray = Array2::default((new_height, new_width));
    // copy old layer into the new one
    new_ndarray
        .slice_mut(s![up..up + ndarray.nrows(), left..left + ndarray.ncols()])
        .assign(ndarray);
    // copy the corner of the old ndarray into the corner region of the new one
    let corners = vec![
        ((0..up, 0..left), ndarray[(0, 0)]), // top left
        (
            (0..up, new_width - right..new_width),
            ndarray[(0, old_width - 1)],
        ), // top right
        (
            (new_height - down..new_height, 0..left),
            ndarray[(old_height - 1, 0)],
        ), // bottom
        (
            (new_height - down..new_height, new_width - right..new_width),
            ndarray[(old_height - 1, old_width - 1)],
        ),
    ];
    for ((y_slice, x_slice), corner) in corners {
        new_ndarray
            .slice_mut(s![y_slice, x_slice])
            .map_inplace(|e| *e = corner);
    }
    // copy the old outermost rows/columns into the new area
    let vertical_edge_ranges = vec![
        (ndarray.row(0), (0..up, left..left + old_width)), // upper edge
        (
            ndarray.row(old_height - 1),
            (up + old_height..new_height, left..left + old_width),
        ), // lower edge
    ];
    let horizontal_edge_ranges = vec![
        (ndarray.column(0), (up..up + old_height, 0..left)), // left edge
        (
            ndarray.column(old_width - 1),
            (up..up + old_height, left + old_width..new_width),
        ), // right edge
    ];
    for (edge, (y_slice, x_slice)) in vertical_edge_ranges {
        new_ndarray
            .slice_mut(s![y_slice, x_slice])
            .rows_mut()
            .into_iter()
            .for_each(|mut row| row.assign(&edge))
    }
    for (edge, (y_slice, x_slice)) in horizontal_edge_ranges {
        new_ndarray
            .slice_mut(s![y_slice, x_slice])
            .columns_mut()
            .into_iter()
            .for_each(|mut row| row.assign(&edge))
    }
    Some(new_ndarray)
}

fn extend_layer<T: TilemapLayer>(
    layer: &mut T,
    up_left: Point<usize>,
    down_right: Point<usize>,
) -> Option<()> {
    *layer.tiles_mut().unwrap_mut() =
        edge_extend_ndarray(layer.tiles().unwrap_ref(), up_left, down_right)?;
    Some(())
}

impl QuadsLayer {
    // TODO: somehow make atomic + the return value reasonable
    fn shift(&mut self, offset: Point<I17F15>) -> Option<()> {
        for quad in &mut self.quads {
            quad.position = quad.position.checked_add(offset)?;
            for corner in &mut quad.corners {
                *corner = corner.checked_add(offset)?;
            }
        }
        Some(())
    }
}

impl SoundsLayer {
    // TODO: somehow make the return value reasonable
    fn shift(&mut self, offset: Point<I17F15>) -> Option<()> {
        for source in &mut self.sources {
            source.position = source.position.checked_add(offset)?;
        }
        Some(())
    }
}

impl TwMap {
    /// Increases the size of the tilemap layers by the specified tile amount in each direction.
    /// To keep the look of the map the same, this also moves the quads in quads layers and adjust the offset of groups as well as their clipping.
    /// Returns None if an overflow occurred.
    #[must_use]
    pub fn extend_layers(mut self, up_left: Point<u16>, down_right: Point<u16>) -> Option<TwMap> {
        let tiles_up_left = up_left.try_into()?;
        let tiles_down_right = down_right.try_into()?;
        let object_offset = Point::try_from(up_left)?;
        for group in &mut self.groups {
            for layer in &mut group.layers {
                use Layer::*;
                match layer {
                    Game(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Tiles(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Quads(l) => l.shift(object_offset)?,
                    Front(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Tele(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Speedup(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Switch(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Tune(l) => extend_layer(l, tiles_up_left, tiles_down_right)?,
                    Sounds(l) => l.shift(object_offset)?,
                    Invalid(_) => {}
                }
            }
            let group_up_left = Point::try_from(up_left)?;
            let group_up_left_parallaxed = group_up_left
                .checked_mul_int(group.parallax)?
                .checked_div_int(Point::PARALLAX_DIVISOR)?;
            // TODO: why add with but also without parallax
            group.offset = group
                .offset
                .checked_add(group_up_left)?
                .checked_sub(group_up_left_parallaxed)?;
            if !group.is_physics_group() {
                group.clip = group.clip.checked_add(group_up_left)?;
            }
        }
        Some(self)
    }
}

/// up - down - left - right
pub fn shrink_ndarray<T: Default + Clone>(
    ndarray: &Array2<T>,
    up_left: Point<usize>,
    down_right: Point<usize>,
) -> Option<Array2<T>> {
    let bottom_right = Point::new(
        ndarray.ncols().checked_sub(down_right.x)?,
        ndarray.nrows().checked_sub(down_right.y)?,
    );
    Some(
        ndarray
            .slice(s![up_left.y..bottom_right.y, up_left.x..bottom_right.x])
            .to_owned(),
    )
}

fn checked_shrink<T: TilemapLayer>(
    mut layer: T,
    up_left: Point<usize>,
    down_right: Point<usize>,
) -> Option<T> {
    *layer.tiles_mut().unwrap_mut() =
        shrink_ndarray(layer.tiles().unwrap_ref(), up_left, down_right)?;
    Some(layer)
}

impl Layer {
    fn shrink(self, up_left: Point<usize>, down_right: Point<usize>) -> Option<Layer> {
        use Layer::*;
        let offset = up_left.try_into()?.checked_mul_int(Point::new_same(-1))?;
        Some(match self {
            Game(l) => Game(checked_shrink(l, up_left, down_right)?),
            Tiles(l) => Tiles(checked_shrink(l, up_left, down_right)?),
            Quads(mut l) => {
                l.shift(offset)?;
                Quads(l)
            }
            Front(l) => Front(checked_shrink(l, up_left, down_right)?),
            Tele(l) => Tele(checked_shrink(l, up_left, down_right)?),
            Speedup(l) => Speedup(checked_shrink(l, up_left, down_right)?),
            Switch(l) => Switch(checked_shrink(l, up_left, down_right)?),
            Tune(l) => Tune(checked_shrink(l, up_left, down_right)?),
            Sounds(mut l) => {
                l.shift(offset)?;
                Sounds(l)
            }
            Invalid(l) => Invalid(l),
        })
    }
}

/// For every direction, this function will return the amount of rows/columns that are duplicates of the innermost one.
/// Use this function with caution, since for example layers with the same tile everywhere can be shrunken into nothingness in each direction.
fn lossless_shrink_distances<T: Default + Clone + PartialEq>(
    ndarray: &Array2<T>,
) -> (Point<usize>, Point<usize>) {
    let rows: Vec<_> = ndarray.rows().into_iter().collect();
    let up = match rows.windows(2).position(|rows| rows[0] != rows[1]) {
        Some(pos) => pos,
        None => ndarray.nrows(),
    };
    let down = match rows.windows(2).rev().position(|rows| rows[0] != rows[1]) {
        Some(pos) => pos,
        None => ndarray.nrows(),
    };

    let columns: Vec<_> = ndarray.columns().into_iter().collect();
    let left = match columns.windows(2).position(|rows| rows[0] != rows[1]) {
        Some(pos) => pos,
        None => ndarray.ncols(),
    };
    let right = match columns.windows(2).rev().position(|rows| rows[0] != rows[1]) {
        Some(pos) => pos,
        None => ndarray.ncols(),
    };
    (Point::new(left, up), Point::new(right, down))
}

impl Layer {
    fn lossless_shrink_distances(&self) -> Option<(Point<usize>, Point<usize>)> {
        use Layer::*;
        match self {
            Game(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Tiles(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Quads(_) => None,
            Front(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Tele(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Speedup(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Switch(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Tune(l) => Some(lossless_shrink_distances(l.tiles.unwrap_ref())),
            Sounds(_) => None,
            Invalid(_) => None,
        }
    }
}

impl Group {
    fn lossless_tiles_layer_shrink(mut self) -> Option<Group> {
        if self
            .layers
            .iter()
            .all(|layer| layer.kind() != LayerKind::Tiles)
        {
            return Some(self); // no tiles layer -> nothing to shrink
        }
        let shrink_distances: Vec<_> = self
            .layers
            .iter()
            .map(|layer| layer.lossless_shrink_distances())
            .collect();

        let smallest_layer_height = self
            .layers
            .iter()
            .filter_map(|layer| layer.shape().map(|shape| shape.x))
            .min()
            .unwrap();
        let smallest_layer_width = self
            .layers
            .iter()
            .filter_map(|layer| layer.shape().map(|shape| shape.y))
            .min()
            .unwrap();
        let mut lowest_up_shrink = 0;
        let mut lowest_left_shrink = 0;
        if !self.is_physics_group() {
            // all layers in a group must shrink by the same amount from the left and up, since you can't offset single layers
            lowest_up_shrink = shrink_distances
                .iter()
                .filter_map(|&d| d.map(|dirs| dirs.0.y))
                .min()
                .unwrap();
            lowest_left_shrink = shrink_distances
                .iter()
                .filter_map(|&d| d.map(|dirs| dirs.0.x))
                .min()
                .unwrap();
            // ensure that the layers are at least 2 blocks high and wide
            lowest_up_shrink = min(lowest_up_shrink, smallest_layer_height.checked_sub(2)?);
            lowest_left_shrink = min(lowest_left_shrink, smallest_layer_width.checked_sub(2)?);
        }
        self.layers = self
            .layers
            .into_iter()
            .zip(shrink_distances)
            .map(|(layer, dirs)| {
                if layer.kind().is_physics_layer() {
                    Some(layer)
                } else {
                    let mut down = dirs.map(|d| d.1.y).unwrap_or(0);
                    let mut right = dirs.map(|d| d.1.x).unwrap_or(0);
                    // ensure that the layers are at least 2 blocks high and wide
                    if let Some(size) = layer.shape() {
                        down = min(down, size.x.checked_sub(lowest_up_shrink + 2)?);
                        right = min(right, size.y.checked_sub(lowest_left_shrink + 2)?);
                    }
                    let up_left = Point::new(lowest_left_shrink, lowest_up_shrink);
                    let down_right = Point::new(right, down);
                    layer.shrink(up_left, down_right)
                }
            })
            .collect::<Option<_>>()?;

        // shrinking the layers from up and left moves them, so the offset has to correct that
        self.offset = self
            .offset
            .checked_sub(Point::new(lowest_left_shrink, lowest_up_shrink).try_into()?)?;
        Some(self)
    }

    fn lossless_layer_shrink(mut self) -> Option<Group> {
        if self
            .layers
            .iter()
            .all(|layer| !layer.kind().is_tile_map_layer())
        {
            return Some(self); // no tilemap layer -> nothing to shrink
        }
        let shrink_distances: Vec<_> = self
            .layers
            .iter()
            .map(|layer| layer.lossless_shrink_distances())
            .collect();

        let smallest_layer_height = self
            .layers
            .iter()
            .filter_map(|layer| layer.shape().map(|shape| shape.x))
            .min()
            .unwrap();
        let smallest_layer_width = self
            .layers
            .iter()
            .filter_map(|layer| layer.shape().map(|shape| shape.y))
            .min()
            .unwrap();
        // all layers in a group must shrink by the same amount from the left and up, since you can't offset single layers
        let mut lowest_up_shrink = shrink_distances
            .iter()
            .filter_map(|&d| d.map(|dirs| dirs.0.y))
            .min()
            .unwrap();
        let mut lowest_left_shrink = shrink_distances
            .iter()
            .filter_map(|&d| d.map(|dirs| dirs.0.x))
            .min()
            .unwrap();
        // ensure that the layers are at least 2 blocks high and wide
        lowest_up_shrink = min(lowest_up_shrink, smallest_layer_height.checked_sub(2)?);
        lowest_left_shrink = min(lowest_left_shrink, smallest_layer_width.checked_sub(2)?);
        // all physics layers must shrink by the same amount from the right and down, since they must be the same size
        let physics_layer_down_shrink = self
            .layers
            .iter()
            .filter_map(|l| {
                if l.kind().is_physics_layer() {
                    l.lossless_shrink_distances()
                } else {
                    None
                }
            })
            .map(|d| d.1.y)
            .min();
        let physics_layer_right_shrink = self
            .layers
            .iter()
            .filter_map(|l| {
                if l.kind().is_physics_layer() {
                    l.lossless_shrink_distances()
                } else {
                    None
                }
            })
            .map(|d| d.1.x)
            .min();
        self.layers = self
            .layers
            .into_iter()
            .zip(shrink_distances)
            .map(|(layer, dirs)| {
                let (mut down, mut right) = match layer.kind().is_physics_layer() {
                    true => (
                        physics_layer_down_shrink.unwrap(),
                        physics_layer_right_shrink.unwrap(),
                    ),
                    false => (
                        dirs.map(|d| d.1.y).unwrap_or(0),
                        dirs.map(|d| d.1.x).unwrap_or(0),
                    ),
                };
                // ensure that the layers are at least 2 blocks high and wide
                if let Some(size) = layer.shape() {
                    down = min(down, size.y.checked_sub(lowest_up_shrink + 2)?);
                    right = min(right, size.x.checked_sub(lowest_left_shrink + 2)?);
                }
                let up_left = Point::new(lowest_left_shrink, lowest_up_shrink);
                let down_right = Point::new(right, down);
                layer.shrink(up_left, down_right)
            })
            .collect::<Option<_>>()?;

        // shrinking the layers from up and left moves them, so the offset has to correct that
        self.offset = self
            .offset
            .checked_sub(Point::new(lowest_left_shrink, lowest_up_shrink).try_into()?)?;
        Some(self)
    }
}

impl TwMap {
    /// Downsizes all tilemap layers as much as possible.
    /// Note that this will also reduce the size of physics layers and thereby moves the world border!
    /// Downsizing is possible as long as any outer edge is the same as the it's neighboring row/column.
    /// Because of this, it is suggested to use [`TwMap::edit_tiles`](TwMap::edit_tiles) with [`ZeroAir`](ZeroAir) before using this method.
    ///
    /// Returns None if an overflow occurred.
    #[must_use]
    pub fn lossless_shrink_layers(mut self) -> Option<TwMap> {
        self.groups = self
            .groups
            .into_iter()
            .map(|group| group.lossless_layer_shrink())
            .collect::<Option<_>>()?;
        // when the physics group shrinks from the left or up, it gets offset, so we must correct that
        let physics_group = self.physics_group();
        let offset_shift = physics_group.offset;
        for group in &mut self.groups {
            group.offset = group.offset.checked_sub(
                offset_shift
                    .checked_mul_int(group.parallax)?
                    .checked_div_int(Point::PARALLAX_DIVISOR)?,
            )?;
            if !group.is_physics_group() {
                group.clip = group.clip.checked_add(offset_shift)?;
            }
        }
        Some(self)
    }

    /// Downsizes all tiles layers as much as possible.
    /// Downsizing is possible as long as any outer edge is the same as the it's neighboring row/column.
    /// Because of this, it is suggested to use [`TwMap::edit_tiles`](TwMap::edit_tiles) with [`ZeroAir`](ZeroAir) before using this method.

    /// Returns None if an overflow occurred.
    #[must_use]
    pub fn lossless_shrink_tiles_layers(mut self) -> Option<TwMap> {
        self.groups = self
            .groups
            .into_iter()
            .map(|group| group.lossless_tiles_layer_shrink())
            .collect::<Option<_>>()?;
        Some(self)
    }
}

fn mirror_tiles<T: TileFlipping + Copy>(tiles: &Array2<T>, align_width: i32) -> Array2<T> {
    let align_width = align_width.try_to::<usize>();
    if tiles.ncols() > align_width {
        panic!("Array width too large, can't align to smaller width")
    }
    let max_new_index = align_width - 1;
    let max_old_index = tiles.ncols() - 1;
    Array2::from_shape_fn((tiles.nrows(), align_width), |(new_y, new_x)| {
        let old_x = (max_new_index - new_x).min(max_old_index);
        let mut tile = tiles[(new_y, old_x)];
        tile.mirror();
        tile
    })
}

fn mirror_tile_layer<T: TilemapLayer>(mut layer: T, align_width: i32) -> T
where
    T::TileType: TileFlipping,
{
    let mirrored_tiles = mirror_tiles(layer.tiles().unwrap_ref(), align_width);
    *layer.tiles_mut().unwrap_mut() = mirrored_tiles;
    layer
}

fn rotate_tile_map_right<T: TileFlipping>(tiles: &Array2<T>, align_height: i32) -> Array2<T> {
    let align_height = align_height.try_to::<usize>();
    if tiles.nrows() > align_height {
        panic!("Array width too large, can't align to smaller height")
    }
    let max_new_index = align_height - 1;
    let max_old_index = tiles.nrows() - 1;
    Array2::from_shape_fn((tiles.ncols(), align_height), |(new_y, new_x)| {
        let old_y = (max_new_index - new_x).min(max_old_index);
        let mut rotated = tiles[(old_y, new_y)];
        rotated.rotate_right();
        rotated
    })
}

fn rotate_tile_layer<T: TilemapLayer>(mut layer: T, align_height: i32) -> T
where
    T::TileType: TileFlipping,
{
    let rotated_tiles = rotate_tile_map_right(layer.tiles().unwrap_ref(), align_height);
    *layer.tiles_mut().unwrap_mut() = rotated_tiles;
    layer
}

pub trait TileFlipping: AnyTile {
    fn mirror(&mut self);

    fn rotate_right(&mut self);
}

impl TileFlags {
    fn mirror(&mut self) {
        match self.contains(TileFlags::ROTATE) {
            false => self.toggle(TileFlags::FLIP_V),
            true => self.toggle(TileFlags::FLIP_H),
        }
    }

    // rotates sideways blocks by rotating them by 180°, required for some game/front/switch layer tiles
    fn mirror_rotate(&mut self) {
        if self.contains(TileFlags::ROTATE) {
            self.toggle(TileFlags::FLIP_H | TileFlags::FLIP_V);
        }
    }

    fn rotate_right(&mut self) {
        if self.contains(TileFlags::ROTATE) {
            self.toggle(TileFlags::FLIP_H | TileFlags::FLIP_V);
        }
        self.toggle(TileFlags::ROTATE);
    }
}

impl TileFlipping for Tile {
    fn mirror(&mut self) {
        self.flags.mirror()
    }

    fn rotate_right(&mut self) {
        self.flags.rotate_right();
    }
}

impl TileFlipping for GameTile {
    fn mirror(&mut self) {
        match self.id {
            60 | 61 | 64 | 65 | 67 | 224 | 225 => self.flags.mirror_rotate(),
            _ => self.flags.mirror(),
        }
        // mirror rotating lasers by using the corresponding mirrored tile
        if self.id >= 203 && self.id <= 205 {
            self.id += (206 - self.id) * 2;
        } else if self.id >= 207 && self.id <= 209 {
            self.id -= (self.id - 206) * 2;
        }
    }

    fn rotate_right(&mut self) {
        self.flags.rotate_right();
    }
}

impl TileFlipping for Tele {
    fn mirror(&mut self) {}

    fn rotate_right(&mut self) {}
}

impl TileFlipping for Speedup {
    fn mirror(&mut self) {
        let mut angle = i16::from(self.angle);
        angle = 180 - angle;
        if angle < 0 {
            angle += 360;
        }
        self.angle = angle.into();
    }

    fn rotate_right(&mut self) {
        let mut angle = i16::from(self.angle);
        angle += 90;
        angle %= 360;
        self.angle = angle.into();
    }
}

impl TileFlipping for Switch {
    fn mirror(&mut self) {
        match self.id {
            224 | 225 => self.flags.mirror_rotate(),
            _ => self.flags.mirror(),
        }
        // mirror rotating lasers by using the corresponding mirrored tile
        if self.id >= 203 && self.id <= 205 {
            self.id += (206 - self.id) * 2;
        } else if self.id >= 207 && self.id <= 209 {
            self.id -= (self.id - 206) * 2;
        }
    }

    fn rotate_right(&mut self) {
        self.flags.rotate_right();
    }
}

impl TileFlipping for Tune {
    fn mirror(&mut self) {}

    fn rotate_right(&mut self) {}
}

impl TwMap {
    /// Mirrors the map. Switches left and right.
    /// Returns None if an overflow occurred.
    #[must_use]
    pub fn mirror(mut self) -> Option<TwMap> {
        self.isolate_physics_layers();
        // x_shift is the x-difference between the old and new origin
        let origin_x_shift = self
            .find_physics_layer::<GameLayer>()
            .unwrap()
            .tiles
            .shape()
            .y;
        self.groups = self
            .groups
            .into_iter()
            .map(|group| group.mirror(origin_x_shift.try_to()))
            .collect::<Option<_>>()?;
        self.envelopes = self
            .envelopes
            .into_iter()
            .map(|env| env.mirror())
            .collect::<Option<_>>()?;
        Some(self)
    }

    /// Rotates the map clockwise.
    /// Returns `None` if an overflow occurred.
    #[must_use]
    pub fn rotate_right(mut self) -> Option<TwMap> {
        self.isolate_physics_layers();
        let origin_y_shift = self
            .find_physics_layer::<GameLayer>()
            .unwrap()
            .tiles
            .shape()
            .x;
        self.groups = self
            .groups
            .into_iter()
            .map(|group| group.rotate_right(origin_y_shift.try_to()))
            .collect::<Option<_>>()?;
        self.envelopes = self
            .envelopes
            .into_iter()
            .map(|env| env.rotate_right())
            .collect::<Option<_>>()?;
        Some(self)
    }
}

impl Point<I17F15> {
    fn mirror(&self, origin_x_shift: I17F15) -> Option<Self> {
        Some(Point {
            x: origin_x_shift.checked_sub(self.x)?,
            y: self.y,
        })
    }

    fn rotate_right(&self, origin_y_shift: I17F15) -> Option<Self> {
        Some(Point {
            x: origin_y_shift.checked_sub(self.y)?,
            y: self.x,
        })
    }
}

impl QuadsLayer {
    fn mirror(mut self, align_width: I17F15) -> Option<QuadsLayer> {
        for quad in &mut self.quads {
            quad.position = quad.position.mirror(align_width)?;
            for corner in &mut quad.corners {
                *corner = corner.mirror(align_width)?;
            }
        }
        Some(self)
    }

    fn rotate_right(mut self, align_height: I17F15) -> Option<QuadsLayer> {
        for quad in &mut self.quads {
            quad.position = quad.position.rotate_right(align_height)?;
            for corner in &mut quad.corners {
                *corner = corner.rotate_right(align_height)?;
            }
        }
        Some(self)
    }
}

impl SoundsLayer {
    fn mirror(mut self, align_width: I17F15) -> Option<SoundsLayer> {
        for source in &mut self.sources {
            source.position = source.position.mirror(align_width)?;
        }
        Some(self)
    }

    fn rotate_right(mut self, align_height: I17F15) -> Option<SoundsLayer> {
        for source in &mut self.sources {
            source.position = source.position.rotate_right(align_height)?;
            if let SoundShape::Rectangle { size } = source.shape {
                source.shape = SoundShape::Rectangle { size: size.swap() }
            }
        }
        Some(self)
    }
}

fn calc_new_offset(
    former_offset: I27F5,
    origin_shift: I27F5,
    parallax: i32,
    align_size: I27F5,
) -> Option<I27F5> {
    let origin_shift_parallaxed = origin_shift
        .checked_mul_int(parallax)?
        .checked_div_int(100)?;
    let offset_offset = origin_shift_parallaxed.checked_sub(align_size)?;
    former_offset
        .checked_add(offset_offset)?
        .checked_mul_int(-1)
}

fn calc_new_clip_pos(align_size: I27F5, clip_pos: I27F5, clip_size: I27F5) -> Option<I27F5> {
    let new_clip_corner_pos = clip_pos.checked_add(clip_size)?;
    align_size.checked_sub(new_clip_corner_pos)
}

impl Layer {
    fn mirror(self, align_width: i32) -> Option<Layer> {
        use Layer::*;
        let align_width_fix_15 = I17F15::lossless_try_from(align_width)?;
        assert_eq!(align_width_fix_15.to_bits(), align_width * 32 * 1024);
        Some(match self {
            Game(l) => Game(mirror_tile_layer(l, align_width)),
            Tiles(l) => Tiles(mirror_tile_layer(l, align_width)),
            Quads(l) => Quads(l.mirror(align_width_fix_15)?),
            Front(l) => Front(mirror_tile_layer(l, align_width)),
            Tele(l) => Tele(mirror_tile_layer(l, align_width)),
            Speedup(l) => Speedup(mirror_tile_layer(l, align_width)),
            Switch(l) => Switch(mirror_tile_layer(l, align_width)),
            Tune(l) => Tune(mirror_tile_layer(l, align_width)),
            Sounds(l) => Sounds(l.mirror(align_width_fix_15)?),
            Invalid(l) => Invalid(l),
        })
    }

    fn rotate(self, align_height: i32) -> Option<Layer> {
        use Layer::*;
        let align_height_fix_15 = I17F15::lossless_try_from(align_height)?;
        Some(match self {
            Game(l) => Game(rotate_tile_layer(l, align_height)),
            Tiles(l) => Tiles(rotate_tile_layer(l, align_height)),
            Quads(l) => Quads(l.rotate_right(align_height_fix_15)?),
            Front(l) => Front(rotate_tile_layer(l, align_height)),
            Tele(l) => Tele(rotate_tile_layer(l, align_height)),
            Speedup(l) => Speedup(rotate_tile_layer(l, align_height)),
            Switch(l) => Switch(rotate_tile_layer(l, align_height)),
            Tune(l) => Tune(rotate_tile_layer(l, align_height)),
            Sounds(l) => Sounds(l.rotate_right(align_height_fix_15)?),
            Invalid(l) => Invalid(l),
        })
    }
}

impl Group {
    fn mirror(mut self, origin_x_shift: i32) -> Option<Group> {
        let align_width = match self
            .layers
            .iter()
            .filter_map(|layer| layer.shape())
            .map(|shape| shape.y)
            .max()
        {
            None => origin_x_shift,
            Some(width) => width.try_to::<i32>(),
        };
        let origin_x_shift = I27F5::lossless_try_from(origin_x_shift)?;
        let group_align_width = I27F5::lossless_try_from(align_width)?;
        self.offset.x = calc_new_offset(
            self.offset.x,
            origin_x_shift,
            self.parallax.x,
            group_align_width,
        )?;

        if !self.is_physics_group() {
            self.clip.x = calc_new_clip_pos(origin_x_shift, self.clip.x, self.clip_size.x)?;
        }
        self.layers = self
            .layers
            .into_iter()
            .map(|layer| layer.mirror(align_width))
            .collect::<Option<_>>()?;
        Some(self)
    }

    fn rotate_right(mut self, origin_y_shift: i32) -> Option<Group> {
        let align_height = match self
            .layers
            .iter()
            .filter_map(|layer| layer.shape())
            .map(|shape| shape.x)
            .max()
        {
            None => origin_y_shift,
            Some(height) => height.try_to::<i32>(),
        };

        let origin_y_shift = I27F5::lossless_try_from(origin_y_shift)?;
        let group_align_height = I27F5::lossless_try_from(align_height)?;
        let original_offset_x = self.offset.x;
        self.offset.x = calc_new_offset(
            self.offset.y,
            origin_y_shift,
            self.parallax.y,
            group_align_height,
        )?;
        self.offset.y = original_offset_x;

        if !self.is_physics_group() {
            let original_clip_x = self.clip.x;
            self.clip.x = calc_new_clip_pos(origin_y_shift, self.clip.y, self.clip_size.y)?;
            self.clip.y = original_clip_x;
        }

        self.clip_size = self.clip_size.swap();
        self.parallax = self.parallax.swap();

        self.layers = self
            .layers
            .into_iter()
            .map(|layer| layer.rotate(align_height))
            .collect::<Option<_>>()?;
        Some(self)
    }
}

impl Envelope {
    fn mirror(mut self) -> Option<Envelope> {
        match &mut self {
            Envelope::Position(env) => {
                for point in &mut env.points {
                    point.content.offset.x = point.content.offset.x.checked_mul_int(-1)?;
                    point.content.rotation = point.content.rotation.checked_mul_int(-1)?;
                }
            }
            Envelope::Color(_) => {}
            Envelope::Sound(_) => {}
        }
        Some(self)
    }

    fn rotate_right(mut self) -> Option<Envelope> {
        match &mut self {
            Envelope::Position(env) => {
                for point in &mut env.points {
                    let original_x = point.content.offset.x;
                    point.content.offset.x = point.content.offset.y.checked_mul_int(-1)?;
                    point.content.offset.y = original_x;
                }
            }
            Envelope::Color(_) => {}
            Envelope::Sound(_) => {}
        }
        Some(self)
    }
}

impl TwMap {
    /// Move cosmetic layers from the physics group into separate groups, keeping the render order
    pub fn isolate_physics_layers(&mut self) {
        let index = self
            .groups
            .iter()
            .position(|g| g.is_physics_group())
            .unwrap();
        let game_group = &mut self.groups[index];
        let mut front_group = Group {
            name: String::from("v Front"),
            ..Group::physics()
        };
        let mut back_group = Group {
            name: String::from("^ Back"),
            ..Group::physics()
        };
        let mut i = 0;
        let mut after = false;
        while i < game_group.layers.len() {
            if !game_group.layers[i].kind().is_physics_layer() {
                if after {
                    back_group.layers.push(game_group.layers.remove(i));
                } else {
                    front_group.layers.push(game_group.layers.remove(i));
                }
            } else {
                if game_group.layers[i].kind() == LayerKind::Game {
                    after = true;
                }
                i += 1;
            }
        }
        if !back_group.layers.is_empty() {
            self.groups.insert(index + 1, back_group);
        }
        if !front_group.layers.is_empty() {
            self.groups.insert(index, front_group);
        }
    }
}
