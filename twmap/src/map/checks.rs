use crate::compression::ZlibDecompressionError;
use crate::constants;
use crate::convert::{To, TryTo};
use crate::map::*;

use image::RgbaImage;
use ndarray::Array2;
use thiserror::Error;

use bitflags::bitflags;
use fixed::types::{I17F15, I27F5};
use std::fmt;
use std::mem;

#[derive(Error, Debug)]
#[error("{0}")]
pub enum MapError {
    Vanilla(#[from] VanillaError),
    DDNet(#[from] DDNetError),

    Info(#[from] InfoError),
    Image(#[from] ImageError),
    Envelope(#[from] EnvelopeError),
    Group(#[from] GroupError),
    Layer(#[from] LayerError),
    Sound(#[from] SoundError),
}

#[derive(Error, Debug)]
pub enum DDNetError {
    Bezier,
}

impl fmt::Display for DDNetError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use DDNetError::*;
        match self {
            Bezier => write!(f, "Bezier curves")?,
        }
        write!(f, " are not compatible with DDNet maps")
    }
}

#[derive(Error, Debug)]
pub enum VanillaError {
    InfoSettings,
    DDNetLayer(LayerKind),
    TilesAutoMapper,
    Sounds,
    SoundEnv,
}

impl fmt::Display for VanillaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use VanillaError::*;
        match self {
            InfoSettings => write!(f, "Settings in the map info")?,
            DDNetLayer(kind) => write!(f, "{:?} layers", kind)?,
            TilesAutoMapper => write!(f, "Auto mappers")?,
            Sounds => write!(f, "Map sounds")?,
            SoundEnv => write!(f, "Sound envelopes")?,
        }
        write!(f, " are not compatible with Vanilla maps")
    }
}

#[derive(Error, Debug)]
pub struct AmountError(pub usize);

impl fmt::Display for AmountError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Too many ({}), max is {}", self.0, u16::MAX)
    }
}

pub fn check_amount<T>(items: &[T]) -> Result<(), AmountError> {
    if items.len() > u16::MAX.to::<usize>() {
        Err(AmountError(items.len()))
    } else {
        Ok(())
    }
}

const SANITIZE_OPTIONS: sanitize_filename::Options = sanitize_filename::Options {
    windows: true,
    truncate: true,
    replacement: "",
};

const SANITIZE_CHECK_OPTIONS: sanitize_filename::OptionsForCheck =
    sanitize_filename::OptionsForCheck {
        windows: true,
        truncate: true,
    };

#[derive(Error, Debug)]
pub struct SanitizationError(pub String);

impl fmt::Display for SanitizationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Non-sanitized string: {}, sanitized: {}",
            self.0,
            sanitize_filename::sanitize_with_options(&self.0, SANITIZE_OPTIONS)
        )
    }
}

// used for image and sound names
pub fn check_string_sanitization(string: &str, extension: &str) -> Result<(), SanitizationError> {
    let mut filename = string.to_string();
    filename.push('.');
    filename.push_str(extension);
    if !sanitize_filename::is_sanitized_with_options(&filename, SANITIZE_CHECK_OPTIONS) {
        Err(SanitizationError(filename))
    } else {
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct StringLenError {
    pub string: String,
    pub max_length: usize,
}

impl fmt::Display for StringLenError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "String '{}' is {} bytes long, max is {}",
            self.string,
            self.string.len(),
            self.max_length
        )
    }
}

pub fn check_string_length(string: &str, max_length: usize) -> Result<(), StringLenError> {
    if string.len() > max_length {
        Err(StringLenError {
            string: string.to_string(),
            max_length,
        })
    } else {
        Ok(())
    }
}

impl TwMap {
    pub fn check(&self) -> Result<(), MapError> {
        self.check_info()?;
        self.check_images()?;
        self.check_envelopes()?;
        self.check_groups()?;
        self.check_layers()?;
        self.check_sounds()?;

        match self.version {
            Version::Teeworlds07 => self.check_vanilla()?,
            Version::DDNet06 => self.check_ddnet()?,
        }

        Ok(())
    }

    pub fn check_ddnet(&self) -> Result<(), DDNetError> {
        use DDNetError::*;
        if is_bezier_used(&self.envelopes) {
            return Err(Bezier);
        }
        Ok(())
    }

    pub fn check_vanilla(&self) -> Result<(), VanillaError> {
        use VanillaError::*;
        if !self.info.settings.is_empty() {
            return Err(InfoSettings);
        }
        for group in &self.groups {
            for layer in &group.layers {
                if layer.is_ddnet_layer() {
                    return Err(DDNetLayer(layer.kind()));
                }
                if let Layer::Tiles(layer) = layer {
                    if layer.automapper_config != AutomapperConfig::default() {
                        return Err(TilesAutoMapper);
                    }
                }
            }
        }
        if !self.sounds.is_empty() {
            return Err(Sounds);
        }
        if self
            .envelopes
            .iter()
            .any(|env| env.kind() == EnvelopeKind::Sound)
        {
            return Err(SoundEnv);
        }
        Ok(())
    }

    pub fn check_individually(&mut self) -> Vec<MapError> {
        let mut errors = Vec::new();
        if let Err(err) = self.check_info() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_images() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_envelopes() {
            errors.push(err.into());
        }
        if let Err(err) = self.check_groups() {
            errors.push(err.into());
        } else {
            // check_layers relies on game layer check from check_groups
            if let Err(err) = self.check_layers() {
                errors.push(err.into());
            }
        }
        if let Err(err) = self.check_sounds() {
            errors.push(err.into());
        }
        match self.version {
            Version::Teeworlds07 => {
                if let Err(err) = self.check_vanilla() {
                    errors.push(err.into());
                }
            }
            Version::DDNet06 => {
                if let Err(err) = self.check_ddnet() {
                    errors.push(err.into());
                }
            }
        }
        errors
    }

    pub fn check_info(&self) -> Result<(), InfoError> {
        self.info.check()
    }

    pub fn check_images(&self) -> Result<(), ImageError> {
        Image::check_all(&self.images, self.version)
    }

    pub fn check_envelopes(&self) -> Result<(), EnvelopeError> {
        Envelope::check_all(&self.envelopes)
    }

    pub fn check_groups(&self) -> Result<(), GroupError> {
        Group::check_all(&self.groups)
    }

    pub fn check_layers(&self) -> Result<(), LayerError> {
        Layer::check_all(
            &self.groups,
            &self.envelopes,
            &self.images,
            self.sounds.len(),
        )
    }

    pub fn check_sounds(&self) -> Result<(), SoundError> {
        Sound::check_all(&self.sounds)
    }
}

pub trait CheckData {
    type ErrorKind: From<Self::CompressedErrorKind>;
    type CompressedErrorKind;

    fn check_loaded_data(&self) -> Result<(), Self::ErrorKind>;

    fn check_compressed_data(&self) -> Result<(), Self::CompressedErrorKind>;

    fn check_data(&self) -> Result<(), Self::ErrorKind> {
        self.check_compressed_data()?;
        self.check_loaded_data()?;
        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum InfoError {
    #[error("Info error: Author - {0}")]
    AuthorLength(StringLenError),
    #[error("Info error: Author - {0}")]
    VersionLength(StringLenError),
    #[error("Info error: Author - {0}")]
    CreditsLength(StringLenError),
    #[error("Info error: Author - {0}")]
    LicenseLength(StringLenError),
    #[error("Info error: Setting {1} - {0}")]
    SettingLength(StringLenError, usize),
}

impl Info {
    pub fn check(&self) -> Result<(), InfoError> {
        check_string_length(&self.author, Info::MAX_AUTHOR_LENGTH)
            .map_err(InfoError::AuthorLength)?;
        check_string_length(&self.version, Info::MAX_VERSION_LENGTH)
            .map_err(InfoError::VersionLength)?;
        check_string_length(&self.credits, Info::MAX_CREDITS_LENGTH)
            .map_err(InfoError::CreditsLength)?;
        check_string_length(&self.license, Info::MAX_LICENSE_LENGTH)
            .map_err(InfoError::LicenseLength)?;

        for (i, setting) in self.settings.iter().enumerate() {
            check_string_length(setting, Info::MAX_SETTING_LENGTH)
                .map_err(|err| InfoError::SettingLength(err, i))?;
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct ImageError {
    pub index: Option<usize>,
    pub kind: ImageErrorKind,
}

#[derive(Error, Debug)]
pub enum ImageErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameSanitization(#[from] SanitizationError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    Compressed(#[from] CompressedImageError),
    #[error("'{0}' is not a valid external image in version {1:?}")]
    InvalidExternalImage(String, Version),
    #[error("Width or height is too big: {0}, max is {}", i32::MAX)]
    TooLarge(Point<u32>),
    #[error("Width or height is zero: {0}")]
    ZeroSized(Point<u32>),
}

#[derive(Error, Debug)]
pub enum CompressedImageError {
    #[error("Data size ({0}) is invalid, expected is width {} * height {} * 4 = {}", .1.size.x, .1.size.y, .1.size.x.to::<i64>() * .1.size.y.to::<i64>() * 4)]
    WrongCompressedDataSize(usize, ImageLoadInfo),
    #[error("zlib decompression failed - {0}")]
    ZLibDecompression(#[from] ZlibDecompressionError),
}

impl fmt::Display for ImageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Image error")?;
        if let Some(index) = self.index {
            write!(f, " at index {} ", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl CheckData for CompressedData<RgbaImage, ImageLoadInfo> {
    type ErrorKind = ImageErrorKind;
    type CompressedErrorKind = CompressedImageError;

    fn check_loaded_data(&self) -> Result<(), ImageErrorKind> {
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), CompressedImageError> {
        use CompressedImageError::*;
        if let CompressedData::Compressed(_, data_size, info) = self {
            let expected_size = info.size.x.to::<u64>() * info.size.y.to::<u64>() * 4;
            if (*data_size).try_to::<u64>() != expected_size {
                return Err(WrongCompressedDataSize(*data_size, *info));
            }
        }
        Ok(())
    }
}

impl Image {
    pub fn check_all(images: &[Image], version: Version) -> Result<(), ImageError> {
        check_amount(images).map_err(|err| ImageError {
            index: None,
            kind: err.into(),
        })?;
        for (i, image) in images.iter().enumerate() {
            image.check(version).map_err(|kind| ImageError {
                index: Some(i),
                kind,
            })?;
        }
        Ok(())
    }

    pub fn check(&self, version: Version) -> Result<(), ImageErrorKind> {
        check_string_sanitization(self.name(), "png")?;
        check_string_length(self.name(), Image::MAX_NAME_LENGTH)?;

        self.size()
            .try_into::<i32>()
            .ok_or_else(|| ImageErrorKind::TooLarge(self.size()))?;

        match self {
            Image::Embedded(emb) => emb.image.check_data()?,
            Image::External(ex) => {
                if !constants::is_external_name(&ex.name, version) {
                    return Err(ImageErrorKind::InvalidExternalImage(
                        ex.name.clone(),
                        version,
                    ));
                }
            }
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct EnvelopeError {
    pub kind: EnvelopeErrorKind,
    pub index: Option<usize>,
    pub envelope_kind: Option<EnvelopeKind>,
}

#[derive(Error, Debug)]
pub enum EnvelopeErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    EnvPoint(#[from] EnvPointError),
}

#[derive(Error, Debug)]
pub struct EnvPointError {
    pub kind: EnvPointErrorKind,
    pub index: usize,
}

#[derive(Error, Debug)]
pub enum EnvPointErrorKind {
    #[error("Curve type with id {0} is unknown")]
    UnknownCurveType(i32),
    #[error("Timestamp is at {1} while the one before was at {0}")]
    Ordering(i32, i32),
    #[error("Timestamp is negative, must be >= 0")]
    NegativeTimeStamp(i32),
}

impl fmt::Display for EnvPointError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Faulty envelope point at index {} - {}",
            self.index, self.kind
        )
    }
}

impl fmt::Display for EnvelopeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(kind) = self.envelope_kind {
            write!(f, "{:?} ", kind)?;
        }
        write!(f, "envelope error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl<T: Copy> EnvPoint<T> {
    pub fn check_all(points: &[Self]) -> Result<(), EnvPointError> {
        use EnvPointErrorKind::*;
        if let Some(point) = points.first() {
            if point.time < 0 {
                return Err(EnvPointError {
                    kind: NegativeTimeStamp(point.time),
                    index: 0,
                });
            }
        }

        for (i, pair) in points.windows(2).enumerate() {
            if pair[0].time > pair[1].time {
                return Err(EnvPointError {
                    kind: Ordering(pair[0].time, pair[1].time),
                    index: i + 1,
                });
            }
        }

        use CurveKind::*;
        for (i, point) in points.iter().enumerate() {
            match point.curve {
                Step | Linear | Fast | Smooth | Slow | Bezier(_) => {}
                Unknown(n) => {
                    return Err(EnvPointError {
                        kind: UnknownCurveType(n),
                        index: i,
                    })
                }
            }
        }
        Ok(())
    }
}

pub fn is_bezier_used(envelopes: &[Envelope]) -> bool {
    for envelope in envelopes {
        match envelope {
            Envelope::Position(env) => {
                for point in &env.points {
                    if let CurveKind::Bezier(_) = point.curve {
                        return true;
                    }
                }
            }
            Envelope::Color(env) => {
                for point in &env.points {
                    if let CurveKind::Bezier(_) = point.curve {
                        return true;
                    }
                }
            }
            Envelope::Sound(env) => {
                for point in &env.points {
                    if let CurveKind::Bezier(_) = point.curve {
                        return true;
                    }
                }
            }
        }
    }
    false
}

impl Envelope {
    pub fn check(&self) -> Result<(), EnvelopeErrorKind> {
        match self {
            Envelope::Color(env) => {
                EnvPoint::check_all(&env.points)?;
            }
            Envelope::Position(env) => {
                EnvPoint::check_all(&env.points)?;
            }
            Envelope::Sound(env) => {
                EnvPoint::check_all(&env.points)?;
            }
        }
        check_string_length(self.name(), Envelope::MAX_NAME_LENGTH)?;

        Ok(())
    }

    pub fn check_all(envelopes: &[Envelope]) -> Result<(), EnvelopeError> {
        check_amount(envelopes).map_err(|err| EnvelopeError {
            kind: err.into(),
            index: None,
            envelope_kind: None,
        })?;
        for (i, env) in envelopes.iter().enumerate() {
            env.check().map_err(|kind| EnvelopeError {
                kind,
                index: Some(i),
                envelope_kind: Some(env.kind()),
            })?;
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct GroupError {
    pub index: Option<usize>,
    pub kind: GroupErrorKind,
}

#[derive(Error, Debug)]
pub enum GroupErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("The physics group is missing")]
    NoPhysicsGroup,
    #[error("More than one physics group: {0}")]
    MultiplePhysicsGroups(u16),
    #[error("Physics group has non-default values")]
    PhysicsGroupValues,
}

impl fmt::Display for GroupError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Group error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl From<GroupErrorKind> for GroupError {
    fn from(kind: GroupErrorKind) -> Self {
        GroupError { index: None, kind }
    }
}

impl Group {
    pub fn check(&self) -> Result<(), GroupErrorKind> {
        check_string_length(&self.name, Group::MAX_NAME_LENGTH)?;
        Ok(())
    }

    pub fn check_all(groups: &[Group]) -> Result<(), GroupError> {
        check_amount(groups).map_err(GroupErrorKind::from)?;
        for (i, group) in groups.iter().enumerate() {
            group.check().map_err(|kind| GroupError {
                index: Some(i),
                kind,
            })?;
        }
        Group::check_physics_group(groups)?;
        Ok(())
    }

    // the variables of the physics group should not be edited
    pub fn check_physics_group_values(&self) -> Result<(), GroupErrorKind> {
        use GroupErrorKind::*;
        if self.offset.x != 0
            || self.offset.y != 0
            || self.parallax.x != 100
            || self.parallax.y != 100
            || self.clipping
            || self.clip.x != 0
            || self.clip.y != 0
            || self.clip_size.x != 0
            || self.clip_size.y != 0
            || self.name != "Game"
        {
            Err(PhysicsGroupValues)
        } else {
            Ok(())
        }
    }

    /// Ensure that there is exactly one physics group and that it has the correct values
    pub fn check_physics_group(groups: &[Group]) -> Result<(), GroupError> {
        use GroupErrorKind::*;
        // ensure exactly one physics group
        let mut physics_group = None;
        let mut physics_group_index = 0;
        let mut physics_group_count = 0;

        for (i, group) in groups.iter().enumerate() {
            if group
                .layers
                .iter()
                .any(|layer| layer.kind().is_physics_layer())
            {
                physics_group = Some(group);
                physics_group_index = i;
                physics_group_count += 1;
            }
        }

        if physics_group_count == 0 {
            return Err(NoPhysicsGroup.into());
        }
        if physics_group_count > 1 {
            return Err(MultiplePhysicsGroups(physics_group_count).into());
        }

        physics_group
            .unwrap()
            .check_physics_group_values()
            .map_err(|kind| GroupError {
                index: Some(physics_group_index),
                kind,
            })?;

        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct OutOfBoundsError(pub usize);

impl fmt::Display for OutOfBoundsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "is out of bounds, there are only {}", self.0)
    }
}

pub fn check_bound(index: u16, len: usize) -> Result<(), OutOfBoundsError> {
    if usize::from(index) >= len {
        Err(OutOfBoundsError(len))
    } else {
        Ok(())
    }
}

pub fn check_opt_bound(index: Option<u16>, len: usize) -> Result<(), OutOfBoundsError> {
    if let Some(index) = index {
        if usize::from(index) >= len {
            return Err(OutOfBoundsError(len));
        }
    }
    Ok(())
}

#[derive(Error, Debug)]
pub struct ReferencedEnvelopeError {
    pub index: u16,
    pub env_kind: EnvelopeKind,
    pub kind: ReferencedEnvelopeErrorKind,
}

impl fmt::Display for ReferencedEnvelopeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Referenced {:?} envelope at index {} {}",
            self.env_kind, self.index, self.kind
        )
    }
}

#[derive(Error, Debug)]
pub enum ReferencedEnvelopeErrorKind {
    #[error("{0}")]
    OutOfBounds(#[from] OutOfBoundsError),
    #[error("is actually an {0:?} envelope")]
    WrongKind(EnvelopeKind),
}

pub fn check_envelope_index(
    index: Option<u16>,
    env_kind: EnvelopeKind,
    envelopes: &[Envelope],
) -> Result<(), ReferencedEnvelopeError> {
    check_envelope_index_helper(index, env_kind, envelopes).map_err(|kind| {
        ReferencedEnvelopeError {
            index: index.unwrap(),
            env_kind,
            kind,
        }
    })
}

fn check_envelope_index_helper(
    index: Option<u16>,
    kind: EnvelopeKind,
    envelopes: &[Envelope],
) -> Result<(), ReferencedEnvelopeErrorKind> {
    if let Some(index) = index {
        check_bound(index, envelopes.len())?;
        if envelopes[index.to::<usize>()].kind() != kind {
            return Err(ReferencedEnvelopeErrorKind::WrongKind(
                envelopes[index.to::<usize>()].kind(),
            ));
        }
    }
    Ok(())
}

#[derive(Error, Debug)]
pub struct LayerError {
    pub layer_group: Option<(usize, Option<usize>)>,
    pub layer_kind: Option<LayerKind>,
    pub kind: LayerErrorKind,
}

#[derive(Error, Debug)]
pub enum LayerErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("{0}")]
    ReferencedEnvelope(#[from] ReferencedEnvelopeError),
    #[error("Missing")]
    MissingGameLayer,
    #[error("Invalid layer kind")]
    UnknownLayerKind,
    #[error("There is already a physics layer of this kind")]
    DuplicatePhysicsLayer,
    #[error("Different dimensions {0} than the game layer {1}")]
    DifferentPhysicsLayerDimensions(Point<usize>, Point<usize>),
    #[error("Image index {0}")]
    ImageOutOfBounds(OutOfBoundsError),
    #[error("Sound index {0}")]
    SoundOutOfBounds(OutOfBoundsError),
    #[error("Auto mapper seed {0} is invalid, must be less than 1,000,000,0000")]
    AutoMapperSeed(u32),
    #[error("Quad error - {0}")]
    Quad(#[from] QuadError),
    #[error("Sound source error - {0}")]
    SoundSource(#[from] SoundSourceError),
    #[error("Layer is too wide ({0}), max width is {}", i32::MAX)]
    TooBigWidth(usize),
    #[error("Layer is too high ({0}), max height is {}", i32::MAX)]
    TooBigHeight(usize),
    #[error("Layer is not wide enough({0}), min width is 2")]
    TooSmallWidth(usize),
    #[error("Layer is not high enough({0}), min height is 2")]
    TooSmallHeight(usize),
    #[error("Invalid tile - {0}")]
    InvalidTile(#[from] TileError),
    #[error("Image width or height isn't divisible by 16")]
    InvalidImageDimensions,
    #[error("{0}")]
    Compressed(#[from] CompressedLayerError),
}

#[derive(Error, Debug)]
pub enum CompressedLayerError {
    #[error("Compressed data size {0} is invalid: expected is width {} * height {} * tile size {} = {}", (.1).0.x, (.1).0.y, (.1).1, (.1).0.x.to::<u64>() * (.1).0.y.to::<u64>() * (.1).1.try_to::<u64>())]
    UncompressedDataSize(usize, (Point<u32>, usize)),
    #[error("zlib decompression failed: {0}")]
    ZLibDecompression(#[from] ZlibDecompressionError),
    #[error("0.7 decompression failed: {0}")]
    TileData(#[from] TileDataError),
}

impl fmt::Display for LayerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(kind) = self.layer_kind {
            write!(f, "{:?} ", kind)?;
        }
        write!(f, "layer error")?;
        if let Some((layer_index, group_index)) = self.layer_group {
            write!(f, " at index {}", layer_index)?;
            if let Some(group_index) = group_index {
                write!(f, ", in group at index {}", group_index)?;
            }
        }
        write!(f, ": {}", self.kind)
    }
}

impl Layer {
    const fn is_ddnet_layer(&self) -> bool {
        use Layer::*;
        match self {
            Game(_) | Tiles(_) | Quads(_) => false,
            Front(_) | Tele(_) | Speedup(_) | Switch(_) | Tune(_) => true,
            Sounds(_) => true,
            Invalid(_) => false,
        }
    }

    fn check(
        &self,
        duplicates: &mut PhysicsLayers,
        envelopes: &[Envelope],
        images: &[Image],
        sound_count: usize,
    ) -> Result<(), LayerErrorKind> {
        use Layer::*;
        use LayerErrorKind::*;
        match self {
            Game(layer) => layer.check(duplicates),
            Tiles(layer) => layer.check(envelopes, images),
            Quads(layer) => layer.check(envelopes, images.len()),
            Front(layer) => layer.check(duplicates),
            Tele(layer) => layer.check(duplicates),
            Speedup(layer) => layer.check(duplicates),
            Switch(layer) => layer.check(duplicates),
            Tune(layer) => layer.check(duplicates),
            Sounds(layer) => layer.check(envelopes, sound_count),
            Invalid(_) => Err(UnknownLayerKind),
        }
    }

    fn check_all(
        groups: &[Group],
        envelopes: &[Envelope],
        images: &[Image],
        sound_count: usize,
    ) -> Result<(), LayerError> {
        check_amount(
            &groups
                .iter()
                .flat_map(|group| group.layers.iter())
                .collect::<Vec<_>>(),
        )
        .map_err(|err| LayerError {
            layer_group: None,
            layer_kind: None,
            kind: err.into(),
        })?;
        let mut duplicates = PhysicsLayers::empty();
        for (group_index, group) in groups.iter().enumerate() {
            for (layer_index, layer) in group.layers.iter().enumerate() {
                layer
                    .check(&mut duplicates, envelopes, images, sound_count)
                    .map_err(|kind| LayerError {
                        layer_group: Some((layer_index, Some(group_index))),
                        layer_kind: Some(layer.kind()),
                        kind,
                    })?;
            }
        }
        Layer::check_physics_layer_shapes(groups)?;
        Ok(())
    }

    pub fn check_physics_layer_shapes(groups: &[Group]) -> Result<(), LayerError> {
        // figure out physics layer size, which must be uniform
        let expected_shape = match groups
            .iter()
            .flat_map(|group| group.layers.iter())
            .find(|layer| layer.kind() == LayerKind::Game)
        {
            None => {
                return Err(LayerError {
                    layer_group: None,
                    layer_kind: Some(LayerKind::Game),
                    kind: LayerErrorKind::MissingGameLayer,
                })
            }
            Some(layer) => layer.shape().unwrap(),
        };
        for (group_index, group) in groups.iter().enumerate() {
            for (layer_index, layer) in group.layers.iter().enumerate() {
                if layer.kind() == LayerKind::Tiles {
                    continue;
                }
                if let Some(shape) = layer.shape() {
                    if shape != expected_shape {
                        return Err(LayerError {
                            layer_group: Some((layer_index, Some(group_index))),
                            layer_kind: Some(layer.kind()),
                            kind: LayerErrorKind::DifferentPhysicsLayerDimensions(
                                expected_shape,
                                shape,
                            ),
                        });
                    }
                }
            }
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum TileDataError {
    #[error("Data size is not divisible by the size of a single tile")]
    InvalidSize,
    #[error("Too many tiles")]
    TooLarge,
    #[error("Too few tiles")]
    TooSmall,
}

#[derive(Error, Debug)]
pub struct TileError {
    pub position: Point<usize>,
    pub kind: TileErrorKind,
}

#[derive(Error, Debug)]
pub enum TileErrorKind {
    #[error("Skip byte of tile is {0} instead of zero")]
    TileSkip(u8),
    #[error("Unused byte of tile is {0} instead of zero")]
    TileUnused(u8),
    #[error("Unknown tile flags used, flags: {:#010b}", .0)]
    UnknownTileFlags(u8),
    #[error("Opaque tile flag used in physics layer")]
    OpaqueTileFlag,
    #[error("Unused byte of speedup is {0} instead of zero")]
    SpeedupUnused(u8),
    #[error("Angle of speedup is {0}, but should be between 0 and (exclusive) 360")]
    SpeedupAngle(i16),
}

impl fmt::Display for TileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Faulty tile at {} - {}", self.position, self.kind)
    }
}

pub trait TileChecking {
    fn check(&self) -> Result<(), TileErrorKind> {
        Ok(())
    }
}

impl TileFlags {
    pub const fn check(self) -> bool {
        TileFlags::from_bits(self.bits()).is_some()
    }
}

impl TileChecking for Tile {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.skip != 0 {
            return Err(TileSkip(self.skip));
        }
        if self.unused != 0 {
            return Err(TileUnused(self.unused));
        }
        if !self.flags.check() {
            return Err(UnknownTileFlags(self.flags.bits()));
        }
        Ok(())
    }
}

impl TileChecking for GameTile {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.skip != 0 {
            return Err(TileSkip(self.skip));
        }
        if self.unused != 0 {
            return Err(TileUnused(self.unused));
        }
        if !self.flags.check() {
            return Err(UnknownTileFlags(self.flags.bits()));
        }
        if self.flags.contains(TileFlags::OPAQUE) {
            return Err(OpaqueTileFlag);
        }
        Ok(())
    }
}

impl TileChecking for Tele {}

impl TileChecking for Switch {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if !self.flags.check() {
            return Err(UnknownTileFlags(self.flags.bits()));
        }
        if self.flags.contains(TileFlags::OPAQUE) {
            return Err(OpaqueTileFlag);
        }
        Ok(())
    }
}

impl TileChecking for Speedup {
    fn check(&self) -> Result<(), TileErrorKind> {
        use TileErrorKind::*;
        if self.unused_padding != 0 {
            return Err(SpeedupUnused(self.unused_padding));
        }
        let angle = i16::from(self.angle);
        if !(0..360).contains(&angle) {
            return Err(SpeedupAngle(angle));
        }
        Ok(())
    }
}

impl TileChecking for Tune {}

impl<T: TileChecking> CheckData for CompressedData<Array2<T>, TilesLoadInfo> {
    type ErrorKind = LayerErrorKind;
    type CompressedErrorKind = CompressedLayerError;

    fn check_loaded_data(&self) -> Result<(), Self::ErrorKind> {
        use LayerErrorKind::*;

        if let CompressedData::Loaded(tiles) = self {
            if tiles.ncols() > i32::MAX.try_to::<usize>() {
                return Err(TooBigWidth(tiles.ncols()));
            }
            if tiles.nrows() > i32::MAX.try_to::<usize>() {
                return Err(TooBigHeight(tiles.nrows()));
            }
            if tiles.ncols() < 2 {
                return Err(TooSmallWidth(tiles.ncols()));
            }
            if tiles.nrows() < 2 {
                return Err(TooSmallHeight(tiles.nrows()));
            }

            if let Some(err) = tiles
                .indexed_iter()
                .map(|(pos, tile)| {
                    tile.check().map_err(|kind| TileError {
                        position: Point::new(pos.1, pos.0),
                        kind,
                    })
                })
                .find(|result| result.is_err())
            {
                err?;
            }
        }
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), Self::CompressedErrorKind> {
        use CompressedLayerError::*;

        if let CompressedData::Compressed(_, size, info) = self {
            if info.compression {
                // 0.7 compression
                if size % 4 != 0 {
                    return Err(TileData(TileDataError::InvalidSize));
                }
            } else {
                let tile_count = u64::from(info.size.x) * u64::from(info.size.y);
                let expected_size = tile_count * mem::size_of::<T>().try_to::<u64>();
                if expected_size != (*size).try_to::<u64>() {
                    return Err(UncompressedDataSize(
                        *size,
                        (info.size, mem::size_of::<T>()),
                    ));
                }
            }
        }
        Ok(())
    }
}

bitflags! {
    pub struct PhysicsLayers: u8 {
        const GAME = 1 << 0;
        const FRONT = 1 << 1;
        const TELE = 1 << 2;
        const SPEEDUP = 1 << 3;
        const SWITCH = 1 << 4;
        const TUNE = 1 << 5;
    }
}

pub trait PhysicsLayerChecking: TilemapLayer
where
    Self::TileType: TileChecking,
{
    fn check(&self, duplicates: &mut PhysicsLayers) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        self.tiles().check_data()?;

        let flag = match Self::kind() {
            LayerKind::Game => PhysicsLayers::GAME,
            LayerKind::Front => PhysicsLayers::FRONT,
            LayerKind::Tele => PhysicsLayers::TELE,
            LayerKind::Speedup => PhysicsLayers::SPEEDUP,
            LayerKind::Switch => PhysicsLayers::SWITCH,
            LayerKind::Tune => PhysicsLayers::TUNE,
            _ => panic!("Not a physics layer"),
        };

        if duplicates.contains(flag) {
            return Err(DuplicatePhysicsLayer);
        }
        duplicates.insert(flag);
        Ok(())
    }
}

impl<T: PhysicsLayer> PhysicsLayerChecking for T {}

impl AutomapperConfig {
    pub const fn check(&self) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        if self.seed > 1_000_000_000 {
            return Err(AutoMapperSeed(self.seed));
        }
        Ok(())
    }
}

impl TilesLayer {
    pub fn check(&self, envelopes: &[Envelope], images: &[Image]) -> Result<(), LayerErrorKind> {
        check_envelope_index(self.color_env, EnvelopeKind::Color, envelopes)?;
        check_opt_bound(self.image, images.len()).map_err(LayerErrorKind::ImageOutOfBounds)?;
        if let Some(index) = self.image {
            if !images[index.to::<usize>()].for_tilemap() {
                return Err(LayerErrorKind::InvalidImageDimensions);
            }
        }
        self.tiles.check_data()?;
        self.automapper_config.check()?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;

        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct QuadError {
    pub index: usize,
    pub position: Point<I17F15>,
    pub err: ReferencedEnvelopeError,
}

impl fmt::Display for QuadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Faulty quad at index {} and position {} - {}",
            self.index, self.position, self.err
        )
    }
}

impl Quad {
    pub fn check(&self, envelopes: &[Envelope]) -> Result<(), ReferencedEnvelopeError> {
        check_envelope_index(self.position_env, EnvelopeKind::Position, envelopes)?;
        check_envelope_index(self.position_env, EnvelopeKind::Position, envelopes)?;
        Ok(())
    }
}

impl QuadsLayer {
    pub fn check(&self, envelopes: &[Envelope], img_count: usize) -> Result<(), LayerErrorKind> {
        use LayerErrorKind::*;
        for (i, quad) in self.quads.iter().enumerate() {
            quad.check(envelopes).map_err(|err| QuadError {
                index: i,
                position: quad.position,
                err,
            })?;
        }

        check_opt_bound(self.image, img_count).map_err(ImageOutOfBounds)?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;

        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum SoundSourceErrorKind {
    #[error("{0}")]
    ReferencedEnvelope(#[from] ReferencedEnvelopeError),
    #[error("delay is negative: {0}")]
    Delay(i32),
    #[error("width is negative: {0}")]
    Width(I17F15),
    #[error("height is negative: {0}")]
    Height(I17F15),
    #[error("radius is negative: {0}")]
    Radius(I27F5),
}

#[derive(Error, Debug)]
pub struct SoundSourceError {
    pub index: usize,
    pub position: Point<I17F15>,
    pub err: SoundSourceErrorKind,
}

impl fmt::Display for SoundSourceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Faulty sound source at index {} and position {} - {}",
            self.index, self.position, self.err
        )
    }
}

impl SoundSource {
    pub fn check(&self, envelopes: &[Envelope]) -> Result<(), SoundSourceErrorKind> {
        use SoundSourceErrorKind::*;
        if self.delay < 0 {
            return Err(Delay(self.delay));
        }
        check_envelope_index(self.position_env, EnvelopeKind::Position, envelopes)?;
        check_envelope_index(self.sound_env, EnvelopeKind::Sound, envelopes)?;

        match self.shape {
            SoundShape::Rectangle { size } => {
                if size.x < 0 {
                    return Err(Width(size.x));
                }
                if size.y < 0 {
                    return Err(Height(size.y));
                }
            }
            SoundShape::Circle { radius } => {
                if radius < 0 {
                    return Err(Radius(radius));
                }
            }
        }

        Ok(())
    }
}

impl SoundsLayer {
    pub fn check(&self, envelopes: &[Envelope], sound_count: usize) -> Result<(), LayerErrorKind> {
        for (i, source) in self.sources.iter().enumerate() {
            source.check(envelopes).map_err(|err| SoundSourceError {
                index: i,
                position: source.position,
                err,
            })?;
        }

        check_opt_bound(self.sound, sound_count).map_err(LayerErrorKind::SoundOutOfBounds)?;
        check_string_length(&self.name, Layer::MAX_NAME_LENGTH)?;
        Ok(())
    }
}

#[derive(Error, Debug)]
pub struct SoundError {
    pub index: Option<usize>,
    pub kind: SoundErrorKind,
}

#[derive(Error, Debug)]
pub enum SoundErrorKind {
    #[error("{0}")]
    Amount(#[from] AmountError),
    #[error("Name - {0}")]
    NameLength(#[from] StringLenError),
    #[error("Name - {0}")]
    NameSanitization(#[from] SanitizationError),
    #[error("{0}")]
    Compressed(#[from] CompressedSoundError),
    #[error("Invalid sound data: {0}")]
    Opus(#[from] opus_headers::ParseError),
}

#[derive(Error, Debug)]
pub enum CompressedSoundError {
    #[error("zlib decompression failed: {0}")]
    ZLibDecompression(#[from] ZlibDecompressionError),
}

impl fmt::Display for SoundError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Sound error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

impl CheckData for CompressedData<Vec<u8>, ()> {
    type ErrorKind = SoundErrorKind;
    type CompressedErrorKind = CompressedSoundError;

    fn check_loaded_data(&self) -> Result<(), SoundErrorKind> {
        if let CompressedData::Loaded(buf) = self {
            opus_headers::parse_from_read(&buf[..])?;
        }
        Ok(())
    }

    fn check_compressed_data(&self) -> Result<(), CompressedSoundError> {
        Ok(())
    }
}

impl Sound {
    pub fn check(&self) -> Result<(), SoundErrorKind> {
        check_string_sanitization(&self.name, "opus")?;
        check_string_length(&self.name, Sound::MAX_NAME_LENGTH)?;

        self.data.check_data()?;
        Ok(())
    }

    pub fn check_all(sounds: &[Sound]) -> Result<(), SoundError> {
        check_amount(sounds).map_err(|err| SoundError {
            index: None,
            kind: err.into(),
        })?;
        for (i, sound) in sounds.iter().enumerate() {
            sound.check().map_err(|kind| SoundError {
                index: Some(i),
                kind,
            })?;
        }
        Ok(())
    }
}
