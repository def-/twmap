use crate::map::*;

use fixed::traits::{Fixed, LosslessTryFrom, LosslessTryInto};
use fixed::types::{I17F15, I22F10, I27F5};
use image::RgbaImage;
use ndarray::Array2;

use std::fmt;

impl Version {
    /// Environment variable name for the directory of the external mapres
    pub const fn mapres_env(&self) -> &'static str {
        match self {
            Version::DDNet06 => MAPRES_06_ENV,
            Version::Teeworlds07 => MAPRES_07_ENV,
        }
    }
}

impl Info {
    pub const MAX_AUTHOR_LENGTH: usize = 31;
    pub const MAX_VERSION_LENGTH: usize = 15;
    pub const MAX_CREDITS_LENGTH: usize = 127;
    pub const MAX_LICENSE_LENGTH: usize = 31;
    pub const MAX_SETTING_LENGTH: usize = 255;
}

impl Image {
    pub const MAX_NAME_LENGTH: usize = 127;
}

impl Envelope {
    pub const MAX_NAME_LENGTH: usize = 31;
}

impl Group {
    pub const MAX_NAME_LENGTH: usize = 11;
}

impl Layer {
    pub const MAX_NAME_LENGTH: usize = 11;
}

impl Sound {
    pub const MAX_NAME_LENGTH: usize = 127;
}

impl TwMap {
    /// Returns a empty map struct with only the version set.
    pub fn empty(version: Version) -> TwMap {
        TwMap {
            version,
            info: Info::default(),
            images: vec![],
            envelopes: vec![],
            groups: vec![],
            sounds: vec![],
        }
    }

    /// Returns a reference to the physics group.
    pub fn physics_group(&self) -> &Group {
        self.groups
            .iter()
            .find(|group| group.is_physics_group())
            .unwrap()
    }

    /// Returns a mutable reference to the physics group.
    pub fn physics_group_mut(&mut self) -> &mut Group {
        self.groups
            .iter_mut()
            .find(|group| group.is_physics_group())
            .unwrap()
    }
}

impl Group {
    /// Checks if the group contains any physics layers.
    pub fn is_physics_group(&self) -> bool {
        self.layers
            .iter()
            .any(|layer| layer.kind().is_physics_layer())
    }
}

impl From<ExternalImage> for Image {
    fn from(image: ExternalImage) -> Self {
        Image::External(image)
    }
}

impl From<EmbeddedImage> for Image {
    fn from(image: EmbeddedImage) -> Self {
        Image::Embedded(image)
    }
}

impl Image {
    pub const fn name(&self) -> &String {
        match self {
            Image::External(img) => &img.name,
            Image::Embedded(img) => &img.name,
        }
    }

    pub fn name_mut(&mut self) -> &mut String {
        match self {
            Image::External(img) => &mut img.name,
            Image::Embedded(img) => &mut img.name,
        }
    }

    pub fn size(&self) -> Point<u32> {
        match self {
            Image::External(image) => image.size,
            Image::Embedded(image) => match &image.image {
                CompressedData::Compressed(_, _, info) => info.size,
                CompressedData::Loaded(image) => Point::new(image.width(), image.height()),
            },
        }
    }

    pub const fn image(&self) -> Option<&CompressedData<RgbaImage, ImageLoadInfo>> {
        match self {
            Image::External(_) => None,
            Image::Embedded(image) => Some(&image.image),
        }
    }

    pub fn image_mut(&mut self) -> Option<&mut CompressedData<RgbaImage, ImageLoadInfo>> {
        match self {
            Image::External(_) => None,
            Image::Embedded(image) => Some(&mut image.image),
        }
    }

    pub fn for_tilemap(&self) -> bool {
        let size = self.size();
        size.x % 16 == 0 && size.y % 16 == 0
    }
}

impl LayerKind {
    pub const fn is_physics_layer(&self) -> bool {
        use LayerKind::*;
        match self {
            Game | Front | Tele | Speedup | Switch | Tune => true,
            Tiles | Quads | Sounds | Invalid(_) => false,
        }
    }

    pub const fn is_tile_map_layer(&self) -> bool {
        use LayerKind::*;
        match self {
            Game | Tiles | Front | Tele | Speedup | Switch | Tune => true,
            Quads | Sounds | Invalid(_) => false,
        }
    }
}

impl<T> CompressedData<Array2<T>, TilesLoadInfo> {
    /// Returns the (height, width) 2-dimensional array of tiles.
    pub fn shape(&self) -> Point<usize> {
        match self {
            CompressedData::Compressed(_, _, info) => info.size.try_into().unwrap(),
            CompressedData::Loaded(array) => Point::new(array.nrows(), array.ncols()),
        }
    }
}

impl Layer {
    /// Returns an identifier for the type of the layer.
    pub const fn kind(&self) -> LayerKind {
        use Layer::*;
        match self {
            Game(_) => LayerKind::Game,
            Tiles(_) => LayerKind::Tiles,
            Quads(_) => LayerKind::Quads,
            Front(_) => LayerKind::Front,
            Tele(_) => LayerKind::Tele,
            Speedup(_) => LayerKind::Speedup,
            Switch(_) => LayerKind::Switch,
            Tune(_) => LayerKind::Tune,
            Sounds(_) => LayerKind::Sounds,
            Invalid(kind) => LayerKind::Invalid(*kind),
        }
    }

    /// Returns the (height, width) of the 2-dimensional array of tiles, if the contained layer is a tilemap layer.
    pub fn shape(&self) -> Option<Point<usize>> {
        use Layer::*;
        match self {
            Game(l) => Some(l.tiles().shape()),
            Tiles(l) => Some(l.tiles().shape()),
            Quads(_) => None,
            Front(l) => Some(l.tiles().shape()),
            Tele(l) => Some(l.tiles().shape()),
            Speedup(l) => Some(l.tiles().shape()),
            Switch(l) => Some(l.tiles().shape()),
            Tune(l) => Some(l.tiles().shape()),
            Sounds(_) => None,
            Invalid(_) => None,
        }
    }
}

impl Quad {
    /// Creates a rectangle quad at the specified position and size.
    /// Except position and size, it uses the default values of a newly created quad in the editor.
    pub fn new(position: Point<I17F15>, size: Point<I17F15>) -> Option<Self> {
        Some(Quad {
            corners: [
                position.checked_add(size.checked_mul_int(Point::new(-1, -1))?)?,
                position.checked_add(size.checked_mul_int(Point::new(1, -1))?)?,
                position.checked_add(size.checked_mul_int(Point::new(-1, 1))?)?,
                position.checked_add(size.checked_mul_int(Point::new(1, 1))?)?,
            ],
            position: Point::ZERO,
            colors: <[Color; 4]>::default(),
            texture_coords: [
                Point::try_from(Point::<i32>::new(0, 0))?,
                Point::try_from(Point::<i32>::new(1, 0))?,
                Point::try_from(Point::<i32>::new(0, 1))?,
                Point::try_from(Point::<i32>::new(1, 1))?,
            ],
            position_env: None,
            position_env_offset: 0,
            color_env: None,
            color_env_offset: 0,
        })
    }
}

impl Default for Quad {
    /// Default settings of a newly created quad in the editor
    fn default() -> Self {
        Quad::new(Point::ZERO, Point::new_same(I17F15::from_num(2))).unwrap()
    }
}

impl Default for SoundSource {
    /// Default settings of a newly created sound source in the editor
    fn default() -> Self {
        SoundSource {
            position: Point {
                x: I17F15::from_num(0),
                y: I17F15::from_num(0),
            },
            looping: true,
            panning: true,
            delay: 0,
            falloff: 80,
            position_env: None,
            position_env_offset: 0,
            sound_env: None,
            sound_env_offset: 0,
            shape: SoundShape::Circle {
                radius: I27F5::from_bits(1500),
            },
        }
    }
}

impl<T, U> From<T> for CompressedData<T, U> {
    fn from(data: T) -> Self {
        CompressedData::Loaded(data)
    }
}

impl<T, U> CompressedData<T, U> {
    /// Returns a reference to the inner loaded value. Panics if isn't loaded.
    pub const fn unwrap_ref(&self) -> &T {
        match self {
            CompressedData::Compressed(_, _, _) => {
                panic!("Data is still compressed, reference unwrap unsuccessful")
            }
            CompressedData::Loaded(data) => data,
        }
    }

    /// Returns a mutable reference to the inner loaded value. Panics if isn't loaded.
    pub fn unwrap_mut(&mut self) -> &mut T {
        match self {
            CompressedData::Compressed(_, _, _) => {
                panic!("Data is still compressed, mut reference unwrap unsuccessful")
            }
            CompressedData::Loaded(data) => data,
        }
    }

    /// Returns the inner loaded value. Panics if isn't loaded.
    pub fn unwrap(self) -> T {
        match self {
            CompressedData::Compressed(_, _, _) => {
                panic!("Data is still compressed, unwrap unsuccessful")
            }
            CompressedData::Loaded(data) => data,
        }
    }
}

impl Envelope {
    pub const fn kind(&self) -> EnvelopeKind {
        use EnvelopeKind::*;
        match self {
            Envelope::Position(_) => Position,
            Envelope::Color(_) => Color,
            Envelope::Sound(_) => Sound,
        }
    }

    /// Returns a reference to the name of the envelope.
    pub const fn name(&self) -> &String {
        use Envelope::*;
        match self {
            Position(env) => &env.name,
            Color(env) => &env.name,
            Sound(env) => &env.name,
        }
    }

    /// Returns a mutable reference to the name of the envelope.
    pub fn name_mut(&mut self) -> &mut String {
        use Envelope::*;
        match self {
            Position(env) => &mut env.name,
            Color(env) => &mut env.name,
            Sound(env) => &mut env.name,
        }
    }
}

impl<T> Default for CurveKind<T> {
    fn default() -> Self {
        CurveKind::Linear
    }
}

impl Default for Group {
    fn default() -> Self {
        Group {
            offset: Point::ZERO,
            parallax: Point::new_same(100),
            layers: Vec::new(),
            clipping: false,
            clip: Point::ZERO,
            clip_size: Point::ZERO,
            name: String::new(),
        }
    }
}

impl Group {
    /// Constructor for the physics group, make sure not to change any values apart from the layers
    pub fn physics() -> Self {
        Group {
            name: String::from("Game"),
            ..Group::default()
        }
    }
}

impl TilesLayer {
    /// Creates a tiles layer with the default values and the specified dimensions.
    pub fn new(shape: (usize, usize)) -> Self {
        TilesLayer {
            detail: false,
            color: Color::default(),
            color_env: None,
            color_env_offset: 0,
            image: None,
            tiles: Array2::default(shape).into(),
            name: "".to_string(),
            automapper_config: AutomapperConfig::default(),
        }
    }
}

impl Layer {
    /// Returns a reference to the name of the layer.
    pub fn name(&self) -> &str {
        use Layer::*;
        match self {
            Game(_) => LayerKind::Game.static_name(),
            Tiles(l) => &l.name,
            Quads(l) => &l.name,
            Front(_) => LayerKind::Front.static_name(),
            Tele(_) => LayerKind::Tele.static_name(),
            Speedup(_) => LayerKind::Speedup.static_name(),
            Switch(_) => LayerKind::Switch.static_name(),
            Tune(_) => LayerKind::Tune.static_name(),
            Sounds(l) => &l.name,
            Invalid(_) => panic!(),
        }
    }

    /// Returns a mutable reference to the name of the layer, if the layer type supports an editable name.
    pub fn name_mut(&mut self) -> Option<&mut String> {
        use Layer::*;
        match self {
            Game(_) => None,
            Tiles(l) => Some(&mut l.name),
            Quads(l) => Some(&mut l.name),
            Front(_) => None,
            Tele(_) => None,
            Speedup(_) => None,
            Switch(_) => None,
            Tune(_) => None,
            Sounds(l) => Some(&mut l.name),
            Invalid(_) => panic!(),
        }
    }
}

impl Tile {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub const fn new(id: u8, flags: TileFlags) -> Self {
        Tile {
            id,
            flags,
            skip: 0,
            unused: 0,
        }
    }
}

impl GameTile {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub const fn new(id: u8, flags: TileFlags) -> Self {
        GameTile {
            id,
            flags,
            skip: 0,
            unused: 0,
        }
    }
}

impl Speedup {
    /// Constructor, required due to unused bytes which will be set to 0.
    pub fn new(id: u8, force: u8, max_speed: u8, angle: i16) -> Self {
        Speedup {
            force,
            max_speed,
            id,
            unused_padding: 0,
            angle: angle.into(),
        }
    }
}

impl From<i16> for I16 {
    fn from(x: i16) -> Self {
        I16 {
            bytes: x.to_le_bytes(),
        }
    }
}

impl From<I16> for i16 {
    fn from(x: I16) -> Self {
        i16::from_le_bytes(x.bytes)
    }
}

impl Default for Color {
    fn default() -> Self {
        Color {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
        }
    }
}

impl Default for I32Color {
    fn default() -> Self {
        I32Color {
            r: I22F10::from_num(1),
            g: I22F10::from_num(1),
            b: I22F10::from_num(1),
            a: I22F10::from_num(1),
        }
    }
}

impl BezierDefault for I32Color {
    fn bezier_default() -> Self {
        I32Color {
            r: I22F10::from_num(0),
            g: I22F10::from_num(0),
            b: I22F10::from_num(0),
            a: I22F10::from_num(0),
        }
    }
}

impl BezierDefault for Position {}

/// TODO for 0.9.0: Remove this impl block
impl BezierDefault for i32 {}

impl Default for Volume {
    fn default() -> Self {
        Volume(I22F10::from_num(1))
    }
}

impl BezierDefault for Volume {
    fn bezier_default() -> Self {
        Self(I22F10::from_num(0))
    }
}

impl TilemapLayer for TilesLayer {
    type TileType = Tile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for GameLayer {
    type TileType = GameTile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for FrontLayer {
    type TileType = GameTile;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for TeleLayer {
    type TileType = Tele;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for SpeedupLayer {
    type TileType = Speedup;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for SwitchLayer {
    type TileType = Switch;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl TilemapLayer for TuneLayer {
    type TileType = Tune;

    fn tiles(&self) -> &CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &self.tiles
    }

    fn tiles_mut(&mut self) -> &mut CompressedData<Array2<Self::TileType>, TilesLoadInfo> {
        &mut self.tiles
    }
}

impl AnyTile for Tile {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        Some(self.flags)
    }

    fn supports_vanilla_compression() -> bool {
        true
    }
}

impl AnyTile for GameTile {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        Some(self.flags)
    }

    fn supports_vanilla_compression() -> bool {
        true
    }
}

impl AnyTile for Switch {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        Some(self.flags)
    }
}

impl AnyTile for Tele {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        None
    }
}

impl AnyTile for Speedup {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        None
    }
}

impl AnyTile for Tune {
    fn id(&self) -> u8 {
        self.id
    }

    fn flags(&self) -> Option<TileFlags> {
        None
    }
}

impl AnyLayer for GameLayer {
    fn kind() -> LayerKind {
        LayerKind::Game
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Game(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Game(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for TilesLayer {
    fn kind() -> LayerKind {
        LayerKind::Tiles
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tiles(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tiles(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for QuadsLayer {
    fn kind() -> LayerKind {
        LayerKind::Quads
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Quads(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Quads(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for FrontLayer {
    fn kind() -> LayerKind {
        LayerKind::Front
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Front(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Front(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for TeleLayer {
    fn kind() -> LayerKind {
        LayerKind::Tele
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tele(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tele(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for SpeedupLayer {
    fn kind() -> LayerKind {
        LayerKind::Speedup
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Speedup(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Speedup(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for SwitchLayer {
    fn kind() -> LayerKind {
        LayerKind::Switch
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Switch(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Switch(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for TuneLayer {
    fn kind() -> LayerKind {
        LayerKind::Tune
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Tune(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Tune(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl AnyLayer for SoundsLayer {
    fn kind() -> LayerKind {
        LayerKind::Sounds
    }

    fn get(layer: &Layer) -> Option<&Self> {
        if let Layer::Sounds(l) = layer {
            Some(l)
        } else {
            None
        }
    }

    fn get_mut(layer: &mut Layer) -> Option<&mut Self> {
        if let Layer::Sounds(l) = layer {
            Some(l)
        } else {
            None
        }
    }
}

impl PhysicsLayer for GameLayer {}
impl PhysicsLayer for FrontLayer {}
impl PhysicsLayer for TeleLayer {}
impl PhysicsLayer for SpeedupLayer {}
impl PhysicsLayer for SwitchLayer {}
impl PhysicsLayer for TuneLayer {}

impl<T: Copy + fmt::Display> fmt::Display for Point<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "x: {}, y: {}", self.x, self.y)
    }
}

impl<T: Copy> Point<T> {
    #[must_use]
    pub const fn new(x: T, y: T) -> Self {
        Self { x, y }
    }

    #[must_use]
    pub const fn new_same(val: T) -> Self {
        Self { x: val, y: val }
    }

    #[must_use]
    pub fn swap(self) -> Self {
        Self {
            x: self.y,
            y: self.x,
        }
    }

    #[must_use]
    pub fn try_map<U: Copy, F: Fn(T) -> Option<U>>(self, f: F) -> Option<Point<U>> {
        Some(Point {
            x: f(self.x)?,
            y: f(self.y)?,
        })
    }

    #[must_use]
    pub fn try_map_from<U: Copy, F: Fn(U) -> Option<T>>(other: Point<U>, f: F) -> Option<Self> {
        Some(Self {
            x: f(other.x)?,
            y: f(other.y)?,
        })
    }

    #[must_use]
    pub fn zip_map<U: Copy, V: Copy, F: Fn(T, U) -> V>(self, other: Point<U>, f: F) -> Point<V> {
        Point {
            x: f(self.x, other.x),
            y: f(self.y, other.y),
        }
    }

    #[must_use]
    pub fn try_zip_map<U: Copy, V: Copy, F: Fn(T, U) -> Option<V>>(
        self,
        other: Point<U>,
        f: F,
    ) -> Option<Point<V>> {
        Some(Point {
            x: f(self.x, other.x)?,
            y: f(self.y, other.y)?,
        })
    }
}

macro_rules! point_op {
    ($trait_:ident, $func:ident) => {
        impl<T: Copy + std::ops::$trait_<Output = T>> std::ops::$trait_ for Point<T> {
            type Output = Self;

            fn $func(self, rhs: Self) -> Self::Output {
                self.zip_map(rhs, T::$func)
            }
        }
    };
}

point_op!(Add, add);
point_op!(Sub, sub);
point_op!(Mul, mul);
point_op!(Div, div);

macro_rules! point_assign_op {
    ($trait_:ident, $func:ident) => {
        impl<T: Copy + std::ops::$trait_> std::ops::$trait_ for Point<T> {
            fn $func(&mut self, rhs: Self) {
                self.x.$func(rhs.x);
                self.y.$func(rhs.y);
            }
        }
    };
}

point_assign_op!(AddAssign, add_assign);
point_assign_op!(SubAssign, sub_assign);
point_assign_op!(MulAssign, mul_assign);
point_assign_op!(DivAssign, div_assign);

impl<T: Copy + Ord> Point<T> {
    /// Element wise minimum
    pub fn min(self, other: Self) -> Self {
        Self {
            x: self.x.min(other.x),
            y: self.y.min(other.y),
        }
    }

    /// Element wise maximum
    pub fn max(self, other: Self) -> Self {
        Self {
            x: self.x.max(other.x),
            y: self.y.max(other.y),
        }
    }

    /// Element wise clamp
    pub fn clamp(self, min: Self, max: Self) -> Self {
        Self {
            x: self.x.clamp(min.x, max.x),
            y: self.y.clamp(min.y, max.y),
        }
    }
}

impl<T: Copy> Point<T> {
    #[must_use]
    pub fn try_from<U: Copy>(other: Point<U>) -> Option<Self>
    where
        T: LosslessTryFrom<U>,
    {
        Self::try_map_from(other, T::lossless_try_from)
    }

    #[must_use]
    pub fn try_into<U: Copy>(self) -> Option<Point<U>>
    where
        T: LosslessTryInto<U>,
    {
        self.try_map(T::lossless_try_into)
    }
}

impl Point<i32> {
    pub const PARALLAX_DIVISOR: Self = Point::new_same(100);
}

impl<T: Fixed> Point<T> {
    const ZERO: Self = Point::new_same(T::ZERO);

    #[must_use]
    pub fn checked_add(self, other: Self) -> Option<Self> {
        self.try_zip_map(other, T::checked_add)
    }

    #[must_use]
    pub fn checked_sub(self, other: Self) -> Option<Self> {
        self.try_zip_map(other, T::checked_sub)
    }

    /// Component-wise multiplication
    #[must_use]
    pub fn checked_mul_int(self, other: Point<T::Bits>) -> Option<Self> {
        self.try_zip_map(other, T::checked_mul_int)
    }

    /// Component-wise division
    #[must_use]
    pub fn checked_div_int(self, other: Point<T::Bits>) -> Option<Self> {
        self.try_zip_map(other, T::checked_div_int)
    }
}
