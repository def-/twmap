TwMap
===

This repository contains both the [Rust](https://www.rust-lang.org/tools/install) `twmap` library as well a collection of tools that use the library in `twmap-tools`.
For more detailed information for either, **check out their respective README.md!**

Project Structure
---

The repository is a [cargo workspace](https://doc.rust-lang.org/cargo/reference/workspaces.html) containing the `twmap` library as well as some tools making use of the library.
Each have their own subdirectory, however the cargo commands can be used from the root directory to execute them globally.

Python Module
---

There is a python module that wraps the `twmap` library!
Check out its repository [twmap-py](https://gitlab.com/Patiga/twmap-py) for more information.

Credits
---

Thanks to ChillerDragon for sponsoring the creation of the MapDir map format.
