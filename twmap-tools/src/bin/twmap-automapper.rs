use std::collections::HashMap;
use twmap::automapper::Automapper;

use clap::{Args, Parser, Subcommand};

use std::error::Error;
use std::fs;
use std::path::PathBuf;
use twmap::convert::TryTo;
use twmap::{Layer, TwMap};

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(propagate_version = true)]
#[clap(about = "TwMap utilities for automappers: checking and running them")]
struct Cli {
    #[clap(subcommand)]
    commands: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Check(Check),
    Run(Run),
}

#[derive(Args, Debug)]
struct Check {
    #[clap(required = true, value_parser)]
    /// Path to automapper
    automapper: PathBuf,
    #[clap(long, value_parser)]
    print_debug: bool,
}

#[derive(Args, Debug)]
struct Run {
    #[clap(required = true, value_parser)]
    /// Path to map which should be automapped
    map_in: PathBuf,
    #[clap(required = true, value_parser)]
    /// Path to map which should be automapped
    map_out: PathBuf,
    #[clap(required = true, value_parser)]
    /// Path to automapper directory
    automappers: PathBuf,
}

fn main() {
    let cli: Cli = Cli::parse();
    let result = match &cli.commands {
        Commands::Check(check) => check.check(),
        Commands::Run(run) => run.run(),
    };
    if let Err(err) = result {
        println!("Error: {}", err);
    }
}

impl Check {
    fn check(&self) -> Result<(), Box<dyn Error>> {
        let file = fs::read_to_string(&self.automapper)?;
        let automapper_name = self
            .automapper
            .file_stem()
            .ok_or("File name not extractable")?
            .to_str()
            .ok_or("File name contains invalid utf8")?
            .to_owned();
        let automapper = Automapper::parse(automapper_name, &file)?;
        if self.print_debug {
            println!("{:#?}", automapper)
        }
        Ok(())
    }
}

impl Run {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let mut map = TwMap::parse_path(&self.map_in)?;
        map.load()?;
        let mapdir = fs::metadata(&self.map_in)?.file_type().is_dir();

        let mut automappers = HashMap::new();
        let automapper_dir_entries =
            fs::read_dir(&self.automappers)?.collect::<Result<Vec<_>, _>>()?;
        for entry in automapper_dir_entries {
            if !entry.file_type()?.is_file() {
                continue;
            }
            let file_name = PathBuf::from(entry.file_name());
            if file_name.extension() != Some("rules".as_ref()) {
                continue;
            }
            let name = match file_name.file_stem().unwrap().to_str() {
                None => {
                    return Err(format!(
                        "Skipping automapper file {:?}: The file name contains invalid utf-8",
                        file_name
                    )
                    .into())
                }
                Some(name) => name,
            };
            println!("Loading automapper {}", name);
            let file = fs::read_to_string(&entry.path())?;
            let automapper = Automapper::parse(name.to_owned(), &file)?;
            automappers.insert(name.to_owned(), automapper);
        }

        let mut anything_automapped = false;
        let mut deterministic = true;

        for (group_i, group) in map.groups.iter_mut().enumerate() {
            for (layer_i, layer) in group.layers.iter_mut().enumerate() {
                if let Layer::Tiles(l) = layer {
                    print!("Tiles layer in group {}, index {}: ", group_i, layer_i);
                    let image_index: usize = match l.image {
                        None => {
                            println!("Has no image set, skipping");
                            continue;
                        }
                        Some(index) => index.try_to(),
                    };
                    let image = &map.images[image_index];
                    let automapper = match automappers.get(image.name()) {
                        None => {
                            println!(
                                "Uses image with name {}, for which we don't have an automapper",
                                image.name()
                            );
                            continue;
                        }
                        Some(a) => a,
                    };
                    let config_index: usize = match l.automapper_config.config {
                        None => {
                            println!("Doesn't have a automapper config set");
                            continue;
                        }
                        Some(i) => i.try_to(),
                    };
                    let config = match automapper.configs.get(config_index) {
                        None => {
                            println!("Error - Layer uses automapper config at index {}, but there are only {} configs", config_index, automapper.configs.len());
                            continue;
                        }
                        Some(c) => c,
                    };
                    config.run(l.automapper_config.seed.try_to(), l.tiles.unwrap_mut());
                    anything_automapped = true;
                    if l.automapper_config.seed == 0 {
                        println!("Automapped config {} with random seed", &config.name);
                        deterministic = false;
                    } else {
                        println!(
                            "Automapped config {} with seed {}",
                            &config.name, l.automapper_config.seed
                        );
                    }
                }
            }
        }

        if !anything_automapped {
            println!("Nothing was actually automapped");
        } else if deterministic {
            println!("The automapping was entirely deterministic without any random seeds");
        } else {
            println!("The automapping was at least partially random");
        }

        match mapdir {
            false => map.save_file(&self.map_out)?,
            true => map.save_dir(&self.map_out)?,
        };
        Ok(())
    }
}
