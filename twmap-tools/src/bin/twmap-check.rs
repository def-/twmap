use twmap::checks::MapError;
use twmap::datafile::RawDatafile;
use twmap::map_dir::{MapDirError, MapDirErrorKind};
use twmap::parse::MapFromDatafileError;
use twmap::{Error, LoadMultiple, TwMap, Version};

use clap::Parser;
use flexi_logger::{DeferredNow, Logger, Record};

use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fmt;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::process::exit;

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(
    about = "Parses maps and report errors/oddities depending on the verbosity level set. If there are multiple maps passed, the map file name will be printed at the start of each line"
)]
struct Cli {
    #[clap(required = true, value_parser)]
    maps: Vec<PathBuf>,
    /// Level of verbosity: default => errors, -v => also warn logs, -vv => also info logs
    #[clap(long, short = 'v', parse(from_occurrences))]
    verbose: i8,
    /// Only parse the datafile (intermediate representation), not the map
    #[clap(long, value_parser, group = "parse_options")]
    datafile_only: bool,
    /// Only parse the TwMap struct, omit further checks
    #[clap(long, value_parser, group = "parse_options")]
    no_checks: bool,
    /// Don't decompress some chunks of data. If this is not set, they will be loaded if no other errors were found
    #[clap(long, value_parser, group = "parse_options")]
    keep_compressed: bool,
    /// Print a short summary at the end
    #[clap(long, value_parser)]
    summary: bool,
    /// If only one map is passed, still write it's file path in front of lines
    #[clap(long, value_parser)]
    always_file_path: bool,
}

fn main() {
    let cli: Cli = Cli::parse();

    WRITE_FILE_PATH.with(|cell| cell.set(cli.maps.len() >= 2 || cli.always_file_path));

    match cli.verbose {
        1 => {
            Logger::try_with_str("warn")
                .unwrap()
                .log_to_stdout()
                .format(log_format)
                .start()
                .unwrap();
        }
        n if n >= 2 => {
            Logger::try_with_str("info")
                .unwrap()
                .log_to_stdout()
                .format(log_format)
                .start()
                .unwrap();
        }
        _ => {}
    };

    let mut counter = ErrorCounter::default();
    let mut ddnet_count = 0;
    let mut teeworlds_07_count = 0;
    let mut datafile_count = 0; // used only for datafile-only

    for file_path in &cli.maps {
        CURRENT_FILE_PATH.with(|cell| *cell.borrow_mut() = Some(file_path.clone()));
        if cli.datafile_only {
            let data = match fs::read(&file_path) {
                Ok(data) => data,
                Err(error) => {
                    println!("io error on path {:?}: {}", file_path, error);
                    exit(1);
                }
            };
            if let Err(error) = RawDatafile::parse(&data).map(|df| df.to_datafile()) {
                counter.insert(DATAFILE_ERR);
                err(file_path, error);
            } else {
                datafile_count += 1;
            }
            continue;
        }
        match TwMap::parse_path_unchecked(file_path) {
            Ok(mut map) => {
                match map.version {
                    Version::Teeworlds07 => teeworlds_07_count += 1,
                    Version::DDNet06 => ddnet_count += 1,
                }
                if cli.no_checks {
                    continue;
                }
                let errors = map.check_individually();
                for error in &errors {
                    counter.insert(map_err_classification(error));
                    err(file_path, error);
                }
                if !errors.is_empty() || cli.keep_compressed {
                    continue;
                }
                if let Err(error) = map.images.load() {
                    err(file_path, &error);
                    map_err_classification(&error.into());
                }
                if let Err(error) = map.groups.load() {
                    err(file_path, &error);
                    map_err_classification(&error.into());
                }
                if let Err(error) = map.sounds.load() {
                    err(file_path, &error);
                    map_err_classification(&error.into());
                }
            }
            Err(Error::Io(error)) => {
                println!("io error on path {:?}: {}", file_path, error);
                exit(1);
            }
            Err(Error::DatafileParse(error)) => {
                counter.insert(DATAFILE_ERR);
                err(file_path, error);
            }
            Err(Error::MapFromDatafile(error)) => {
                counter.insert(map_from_df_err_classification(&error));
                err(file_path, error);
            }
            Err(Error::Dir(error)) => {
                counter.insert(dir_err_classification(&error));
                err(file_path, error);
            }
            _ => unreachable!(),
        }
    }

    if cli.summary {
        if cli.datafile_only {
            println!("Successfully loaded {} datafiles", datafile_count);
        } else {
            println!(
                "Successfully loaded {} ddnet/0.6 maps and {} teeworlds 0.7 maps",
                ddnet_count, teeworlds_07_count
            );
        }

        counter.print();
    }
}

fn err<T: fmt::Display>(map_name: &PathBuf, err: T) {
    match WRITE_FILE_PATH.with(|b| b.get()) {
        true => println!("{:?}: {}", map_name, err),
        false => println!("{}", err),
    }
}

thread_local! {
    pub static WRITE_FILE_PATH: Cell<bool> = Cell::new(false);
    pub static CURRENT_FILE_PATH: RefCell<Option<PathBuf>> = RefCell::new(None);
}

fn log_format(
    write: &mut dyn io::Write,
    _now: &mut DeferredNow,
    record: &Record,
) -> Result<(), io::Error> {
    match WRITE_FILE_PATH.with(|cell| cell.get()) {
        true => write!(
            write,
            "{:?}: [{}] {}",
            CURRENT_FILE_PATH.with(|cell| cell.borrow().as_ref().unwrap().clone()),
            record.level(),
            &record.args()
        ),
        false => write!(write, "[{}] {}", record.level(), &record.args()),
    }
}

#[derive(Default)]
struct ErrorCounter(HashMap<ErrorOrigin, HashMap<ErrorKind, u64>>);

impl ErrorCounter {
    fn insert(&mut self, error: ErrorClassification) {
        *self
            .0
            .entry(error.origin)
            .or_default()
            .entry(error.kind)
            .or_default() += 1;
    }

    fn print(&self) {
        for origin in ErrorOrigin::ALL {
            if let Some(errors) = self.0.get(&origin) {
                println!("{:?} errors: {}", origin, errors.values().sum::<u64>());
                for kind in ErrorKind::ALL {
                    if let Some(count) = errors.get(&kind) {
                        println!("  {:?}: {}", kind, count);
                    }
                }
            }
        }
    }
}

#[derive(Debug, Copy, Clone, Hash)]
struct ErrorClassification {
    origin: ErrorOrigin,
    kind: ErrorKind,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
enum ErrorOrigin {
    Datafile,
    MapFromDatafile,
    Map,
    MapDir,
}

impl ErrorOrigin {
    const ALL: [ErrorOrigin; 4] = [
        ErrorOrigin::Datafile,
        ErrorOrigin::MapFromDatafile,
        ErrorOrigin::Map,
        ErrorOrigin::MapDir,
    ];
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
enum ErrorKind {
    Other,
    Info,
    Image,
    Envelope,
    Group,
    Layer,
    Sound,
    Version,
    Unlisted,
}

impl ErrorKind {
    const ALL: [ErrorKind; 8] = [
        ErrorKind::Other,
        ErrorKind::Info,
        ErrorKind::Image,
        ErrorKind::Envelope,
        ErrorKind::Group,
        ErrorKind::Layer,
        ErrorKind::Sound,
        ErrorKind::Version,
    ];
}

const DATAFILE_ERR: ErrorClassification = ErrorClassification {
    origin: ErrorOrigin::Datafile,
    kind: ErrorKind::Unlisted,
};

fn map_err_classification(err: &MapError) -> ErrorClassification {
    use MapError::*;
    ErrorClassification {
        origin: ErrorOrigin::Map,
        kind: match err {
            Info(_) => ErrorKind::Info,
            Image(_) => ErrorKind::Image,
            Envelope(_) => ErrorKind::Envelope,
            Group(_) => ErrorKind::Group,
            Layer(_) => ErrorKind::Layer,
            Sound(_) => ErrorKind::Sound,
            Vanilla(_) | DDNet(_) => ErrorKind::Version,
        },
    }
}

fn map_from_df_err_classification(err: &MapFromDatafileError) -> ErrorClassification {
    use MapFromDatafileError::*;
    ErrorClassification {
        origin: ErrorOrigin::MapFromDatafile,
        kind: match err {
            Datafile(_) => return DATAFILE_ERR,
            UuidIndex(_) | MapVersion(_) => ErrorKind::Other,
            Info(_) => ErrorKind::Info,
            Image(_) => ErrorKind::Image,
            Envelope(_) | EnvPoint(_) => ErrorKind::Envelope,
            Group(_) => ErrorKind::Group,
            Layer(_) | AutoMapper(_) => ErrorKind::Layer,
            Sound(_) => ErrorKind::Sound,
        },
    }
}

fn path_err_to_kind(path: &Path) -> ErrorKind {
    let components: Vec<&OsStr> = path.components().map(|comp| comp.as_os_str()).collect();
    if components.contains(&OsStr::new("images")) {
        ErrorKind::Image
    } else if components.contains(&OsStr::new("envelopes")) {
        ErrorKind::Envelope
    } else if components.contains(&OsStr::new("groups")) {
        if components.contains(&OsStr::new("layers")) {
            ErrorKind::Layer
        } else {
            ErrorKind::Group
        }
    } else if components.contains(&OsStr::new("sounds")) {
        ErrorKind::Sound
    } else if components.contains(&OsStr::new("info")) {
        ErrorKind::Info
    } else {
        ErrorKind::Other
    }
}

fn dir_err_classification(err: &MapDirError) -> ErrorClassification {
    use MapDirErrorKind::*;
    ErrorClassification {
        origin: ErrorOrigin::MapDir,
        kind: match err.kind {
            InvalidUtf8 => path_err_to_kind(&err.path),
            PhysicsLayerName(_) => ErrorKind::Layer,
            ColorDepth16 => ErrorKind::Image,
            DuplicateImageName => ErrorKind::Image,
            Io(_) => path_err_to_kind(&err.path),
            Json(_) => path_err_to_kind(&err.path),
            Image(_) => ErrorKind::Image,
        },
    }
}
