use twmap::{Point, TwMap};

use clap::Parser;

use std::path::PathBuf;
use std::process::exit;
use twmap::edit::ZeroAir;

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(about = "Applies complex changes to Teeworlds/DDNet maps")]
struct Cli {
    #[clap(value_parser)]
    input_map: PathBuf,
    #[clap(value_parser)]
    output_map: PathBuf,
    /// Save output map in the 'MapDir' format, a human readable format which uses json, png and opus files
    #[clap(long, value_parser)]
    mapdir: bool,
    /// Moves cosmetic layers out of the physics group into separate ones, keeping the render order
    #[clap(long, value_parser)]
    isolate_physics_layers: bool,
    /// Mirror the map (switches left and right), applies before rotating
    #[clap(long, short = 'm', value_parser)]
    mirror: bool,
    /// Rotate the map clockwise, this flag can be used multiple times
    #[clap(long, short = 'r', parse(from_occurrences))]
    rotate: i8,
    /// Removes all unused layers, groups, envelopes, images and sounds
    #[clap(long, value_parser)]
    remove_everything_unused: bool,
    /// Removes all unused envelopes
    #[clap(long, value_parser)]
    remove_unused_envelopes: bool,
    /// Removes all unused images and sounds
    #[clap(long, value_parser)]
    remove_unused_files: bool,
    /// Extends all tilemap layers upwards
    #[clap(long, default_value_t = 0, value_parser)]
    extend_up: u16,
    /// Extends all tilemap layers downwards
    #[clap(long, default_value_t = 0, value_parser)]
    extend_down: u16,
    /// Extends all tilemap layers left
    #[clap(long, default_value_t = 0, value_parser)]
    extend_left: u16,
    /// Extends all tilemap layers right
    #[clap(long, default_value_t = 0, value_parser)]
    extend_right: u16,
    /// Shrinks all tilemap layers (all but quads layers) as much as possible without changing tile placements. Note that this might move the world border and thereby change floating point behavior
    #[clap(long, value_parser)]
    shrink_layers: bool,
    /// Shrinks all tiles layers as much as possible without changing anything
    #[clap(long, value_parser)]
    shrink_tiles_layers: bool,
}

fn main() {
    let cli: Cli = Cli::parse();

    let mut map = match TwMap::parse_path_unchecked(&cli.input_map) {
        Err(err) => {
            println!("parse error: {}", err);
            exit(1);
        }
        Ok(map) => map,
    };

    if cli.remove_unused_envelopes || cli.remove_everything_unused {
        let removed_envelopes = map.remove_unused_envelopes();
        if removed_envelopes > 0 {
            println!("Removed {} unused envelopes!", removed_envelopes);
        }
    }

    if cli.remove_unused_files || cli.remove_everything_unused {
        let (removed_external, removed_embedded) = map.remove_unused_images();
        if removed_external > 0 {
            println!("Removed {} unused external images!", removed_external);
        }
        if removed_embedded > 0 {
            println!("Removed {} unused embedded images!", removed_embedded);
        }

        let removed_sounds = map.remove_unused_sounds();
        if removed_sounds > 0 {
            println!("Removed {} unused sounds!", removed_sounds);
        }
    }

    if let Err(err) = map.check() {
        println!("Map Error: {}", err);
        exit(1);
    }

    if let Err(err) = map.load() {
        println!("Error on decompression: {}", err);
        exit(1);
    }

    if cli.remove_everything_unused {
        let removed_layers = map.remove_unused_layers();
        if removed_layers > 0 {
            println!("Removed {} empty layers!", removed_layers);
        }

        let removed_groups = map.remove_unused_groups();
        if removed_groups > 0 {
            println!("Removed {} empty groups!", removed_groups);
        }
    }

    if cli.isolate_physics_layers {
        map.isolate_physics_layers();
    }

    if [
        cli.extend_up,
        cli.extend_down,
        cli.extend_left,
        cli.extend_right,
    ]
    .into_iter()
    .any(|n| n != 0)
    {
        map = match map.extend_layers(
            Point::new(cli.extend_left, cli.extend_up),
            Point::new(cli.extend_right, cli.extend_down),
        ) {
            None => {
                println!("An overflow occurred while extending the map.");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if cli.mirror {
        println!("Mirroring the map!");
        map = match map.mirror() {
            None => {
                println!("An overflow occurred while mirroring the map.");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    for i in 0..cli.rotate {
        match i {
            0 => println!("Rotating the map!"),
            _ => println!("Rotating the map! (x{})", i + 1),
        };
        map = match map.rotate_right() {
            None => {
                println!("An overflow occurred while rotating the map.");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if cli.shrink_layers {
        map.edit_tiles::<ZeroAir>();
        map = match map.lossless_shrink_layers() {
            None => {
                println!("Overflow while shrinking layers");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if cli.shrink_tiles_layers {
        map.edit_tiles::<ZeroAir>();
        map = match map.lossless_shrink_tiles_layers() {
            None => {
                println!("Overflow while shrinking tiles layers");
                exit(-1);
            }
            Some(map) => map,
        }
    }

    if let Err(err) = match cli.mapdir {
        true => map.save_dir(&cli.output_map),
        false => map.save_file(&cli.output_map),
    } {
        println!("Failed to save map: {}", err);
        exit(1);
    }
}
