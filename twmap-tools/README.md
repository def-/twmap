TwMap Tools
===

Powered by the [twmap](https://crates.io/crates/twmap) library!

Overview
---

All tools are command line argument based and explain their usage with `--help`.

- `twmap-edit`: Easily convert maps between different formats, with some fancy steps in between like mirroring
- `twmap-extract-files`: Extract the image and sound files from maps
- `twmap-check`: Parse maps with different verbosity levels for debugging purposes
- `twmap-check-ddnet`: Scans DDNet maps for faulty behavior (e.g. wrongly placed hook-through, invalid rotation of game tiles)
- `twmap-fix`: Fix some common issues on maps which are not accepted by other executables
- `twmap-automapper`: For checking automappers and running them on maps

Installation
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.

Simply do `cargo install twmap-tools`

Manual Building
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.

To compile the tools in release mode, execute the following command in this directory or the project root:
```
cargo build --release
```
The executable files will be located at the **project root** in `target/release/`.

To compile the debug build instead, omit the `--release` flag.
Note that debug binaries will be significantly slower!
The executable files of debug builds will be located in `target/debug/`.
